# Orion - Easy to use IPFS desktop client by Siderus

Orion is an easy to use [InterPlanetary File System](http://ipfs.io) desktop
client. It allows you to easily add, share, publish, remove files and manage
the IPFS peers from an UI. It includes all it is needed to get started and works
on every major operative system. It is developed and maintained by
[Siderus](https://siderus.io).

![Screenshots](assets/main-screenshot.png)

## About Orion

Orion source code is divided in multiple components: the
[desktop application](https://gitlab.com/siderus/orion/shell)
and the [browser interface](https://gitlab.com/siderus/orion/ui).
The main goals of this project are:

* Help embracing IPFS by providing an easy to use and intuitive interface
* Help the user understanding IPFS/IPNS and the decentralised web
* Show only what is **important** to the user (Ex: the name of the files/directory, not just CID)
* Have a more responsive and enjoyable experience when using IPFS
* Help the user achieving their goals using IPFS instead of traditional tools (ex: Download, Share, Publish websites)

Read more at: [https://orion.siderus.io/](https://orion.siderus.io/)

Chat with us on the official Matrix room:
[https://matrix.to/#/#siderus-orion:matrix.org](https://matrix.to/#/!NsSfDPEJrJyBUKogDd:matrix.org?via=matrix.org&via=swedneck.xyz&via=linuxgaming.life)

## Download

To download the latest version, you can [check the dedicated page](https://orion.siderus.io/#/download).


We support the all major operative systems: Windows, macOS and GNU/Linux.

## About this repository

This Repository Contains the User Interface of Orion.
It is designed to work on its standalone app, but you can use it inside any
moder browser (should work on Chrome, Firefox and Safari).

You will find the source code of the UI and the logic of the Worker

_Note_: When running in a Browser, it does not offer all the future that the
standalone app offers, for example it is not possible to download a file and
there are no integrations with the operative system.

## Run from source code

If you want to run locally the App from the source code you
need [NodeJS](https://nodejs.org/en/), [Yarn](https://yarnpkg.com), [GNU Make](https://www.gnu.org/software/make/) installed. You should also have an IPFS node running.
Once everythig is available, you can run from a bash shell the
following command:

```shell
make run
```

You may need to allow CORS for the ipfs API. You can do so by running:

```bash
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin "[\"http://localhost:3000\"]"
```

Restarting manually the IPFS daemon migth be required to apply the changes.

## License and Contribution

If you want to help contributing to the project, you can reach out to the
[official chatroom on matrix](https://matrix.to/#/#siderus-orion:matrix.org) or
you can test the beta version (updated weekly) of the app by [downloading it
on the official website](https://orion.siderus.io/#/beta).

By contributing by creating issues and/or merge requests you agree to the
license and you will transfer all the rights to [Siderus OU](https://siderus.io).
This is will help future changes and different kind of contributions to the software.

The current license is [Creative Commons BY-NC-ND v4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). The full licese is specified in the [LICENSE](LICENSE).