import "../../utils/sentry"
import * as knownpeers from "./lib/knownpeers"
import { connectToSiderusNetwork } from "./lib/network"

import { getApiClient } from "./lib/ipfs/api"
import {
  addBootstrapAddr, connectTo,
  getCountPeersConnected, getPeerId,
  getPeersConnected, pingPeer,
} from "./lib/ipfs/peers"
import { getBandwidth } from "./lib/ipfs/stats"

console.log("[Connectivity Worker] Starting")

// Load node modules such as path,fs,etc. under `self.node`
self.importScripts("load-node-modules-into-worker.js")

console.log("[Connectivity Worker] Starting to listen for requests")

if (!navigator.onLine) {
  console.warn("[Connectivity Worker] No internet connection detected")
}

onmessage = (event: MessageEvent) => {
  const request: EventMessage = event.data
  // console.log("[Connectivity Worker] Request from client:", request)

  const reply = (responseValue: any) => {
    const message: EventMessage = {
      id: request.id,
      method: request.method,
      value: responseValue,
    }
    // console.log("[Connectivity Worker] Response to client:", request.method, responseValue)
    postMessage(message)
  }

  const replyWithError = (error: Error) => {
    console.error("[Connectivity Worker] Error during request:", request.method, error.message)
    const message: EventMessage = {
      error: error.message,
      id: request.id,
      method: request.method,
    }
    postMessage(message)
  }

  switch (request.method) {
    case "ping": {
      return getApiClient().then(() => reply("pong")).catch(replyWithError)
    }

    /**
     * Known Peers
     */
    case "knownpeers/ls": {
      return knownpeers.getKnownPeers().then(reply).catch(replyWithError)
    }

    case "knownpeers/add": {
      return knownpeers.addKnownPeer(request.value).then(reply).catch(replyWithError)
    }

    case "knownpeers/rm": {
      return knownpeers.deleteKnownPeer(request.value).then(reply).catch(replyWithError)
    }

    case "siderus/connect": {
      return connectToSiderusNetwork().then(reply).catch(replyWithError)
    }

    /**
     * Stats API Bandwidth
     */
    case "stats/bw": {
      return getBandwidth().then(reply).catch(replyWithError)
    }

    /**
     * Network API
     */
    case "peer/get": {
      return getPeerId().then(reply).catch(replyWithError)
    }

    /**
     * Network API
     */
    case "peer/ping": {
      const { peerId, count = 3 } = request.value
      return pingPeer(peerId, count).then(reply).catch(replyWithError)
    }

    case "swarm/connect": {
      return connectTo(request.value).then(reply).catch(replyWithError)
    }

    case "swarm/peers": {
      return getPeersConnected().then(reply).catch(replyWithError)
    }

    case "swarm/peers_count": {
      return getCountPeersConnected().then(reply).catch(replyWithError)
    }

    case "bootstrap/add": {
      return addBootstrapAddr(request.value).then(reply).catch(replyWithError)
    }

    default: {
      return replyWithError(new Error(`Method not supported: ${request.method}`))
    }
  }
}

console.log("[Connectivity Worker] Ready")
