import "../../utils/sentry"
import * as activities from "./lib/activities"
import { describeElement, getHashProperties } from "./lib/metadata"

import { getApiClient } from "./lib/ipfs/api"
import { getObjectStat } from "./lib/ipfs/dags"
import {
  addFile, downloadFile, importFile, mfsFilesStat, removeFiles,
} from "./lib/ipfs/files"
import { getPeersWithObjectbyHash } from "./lib/ipfs/peers"
import { getPinList } from "./lib/ipfs/pins"

console.log("[Files Worker] Starting")

// Load node modules such as path,fs,etc. under `self.node`
self.importScripts("load-node-modules-into-worker.js")

if (!navigator.onLine) {
  console.warn("[Files Worker] No internet connection detected")
}

onmessage = (event: MessageEvent) => {
  const request: EventMessage = event.data
  // console.log("[Worker] Request from client:", request)

  const reply = (responseValue: any) => {
    const message: EventMessage = {
      id: request.id,
      method: request.method,
      value: responseValue,
    }
    // console.log("[Worker] Response to client:", request.method, responseValue)
    postMessage(message)
  }

  const replyWithError = (error: Error) => {
    console.error("[Files Worker] Error during request:", request.method, error.message)
    const message: EventMessage = {
      error: error.message,
      id: request.id,
      method: request.method,
    }
    postMessage(message)
  }

  switch (request.method) {
    case "ping": {
      return getApiClient().then(() => reply("pong")).catch(replyWithError)
    }

    /**
     * Files and Pins
     */
    case "pin/ls": {
      return getPinList(request.value || null).then(reply).catch(replyWithError)
    }
    case "file/add": {
      return activities.add({
        name: request.value.name,
        status: "in progress",
        type: "file/add",
      })
      // Add the file
      .then((activityUUID) =>
        addFile(request.value)
        .then((addResponse: {hash: string}) => {
          // Mark the activity as completed
          activities.patch({
            link: `/properties/${addResponse.hash}`,
            status: "completed",
            uuid: activityUUID,
          })
          return reply(addResponse)
        }),
      )
      .catch(replyWithError)
    }
    case "file/import": {
      return importFile(request.value).then(reply).catch(replyWithError)
    }
    case "file/peers": {
      return getPeersWithObjectbyHash(request.value).then(reply).catch(replyWithError)
    }
    case "file/stat": {
      return getObjectStat(request.value).then(reply).catch(replyWithError)
    }
    case "file/mfs/stat": {
      return mfsFilesStat(request.value).then(reply).catch(replyWithError)
    }
    case "file/download": {
      return downloadFile(request.value.hash, request.value.path).then(reply).catch(replyWithError)
    }
    case "file/remove": {
      return removeFiles(request.value).then(reply).catch(replyWithError)
    }
    case "file/properties": {
      return getHashProperties(request.value).then(reply).catch(replyWithError)
    }
    case "file/describe": {
      return describeElement(request.value.hash, request.value.description).then(reply).catch(replyWithError)
    }

    default: {
      return replyWithError(new Error(`Method not supported: ${request.method}`))
    }
  }
}

console.log("[Files Worker] Ready")
