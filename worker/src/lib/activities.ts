import uuidv4 from "uuid/v4"
//
import { cleanStore, getStore } from "./datastore"

export const DATASTORE_KEY_ACTIVITIES = "activities"

/**
 * Add a new activity (timestamp and uuid are generated if missing)
 */
export function add(partialActivity: Activity): Promise<string> {
  const newActivity: Activity = Object.assign({
    timestamp: new Date(),
    uuid: uuidv4(),
  }, partialActivity)

  return getStore(DATASTORE_KEY_ACTIVITIES)
    .then((s) => s.setItem(newActivity.uuid, newActivity))
    .then(() => newActivity.uuid)
}

/**
 * Patch an activity by UUID (required property)
 */
export function patch(partialActivity: Activity) {
  const { uuid } = partialActivity
  return getStore(DATASTORE_KEY_ACTIVITIES)
    // Get the previous activity by uuid
    .then((s) => s.getItem(uuid))
    .then((activity: Activity) => {
      // patch it and save it
      const patchedA = Object.assign({}, activity, partialActivity)
      return getStore(DATASTORE_KEY_ACTIVITIES).then((s) => s.setItem(uuid, patchedA))
    })
}

// Get will get all the activities by rebuilding the structure as in initialState
export function get(): Promise<Activity[]> {
  // obtain the store
  return getStore(DATASTORE_KEY_ACTIVITIES)
    .then((s) => {
      // Get the keys and for each key return all the values
      return s.keys()
        // For each key get from the store the values
        .then((keys: string[]) => {
          return Promise.all( keys.map((k) => s.getItem(k)) )
        })
    })
}

// This will get all the activities and mark them as failed if thei are not
// completed
export function markNotCompletedAsFailed(): Promise<void> {
  get().then((activities) => {
    const onGoing = activities.filter((e) => e.status === "in progress")
    const promises = onGoing.map((a: Activity) => {
      a.status = "interrupted"

      return patch(a)
    })
    return Promise.all(promises)
  })

  return Promise.resolve()
}

export function reset(): Promise<any> {
  return cleanStore(DATASTORE_KEY_ACTIVITIES)
}
