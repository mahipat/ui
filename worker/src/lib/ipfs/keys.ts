import { getApiClient } from "./api"

export function getKeys(): Promise<any> {
  return getApiClient()
    .then((client) => client.key.list())
}

export function newKey(name: string): Promise<any> {
  return getApiClient()
    .then((client) => {
      const options = {
        size: 2048,
        type: "rsa",
      }

      return client.key.gen(name, options)
    })
}

export function renameKey(oldName: string, newName: string): Promise<any> {
  return getApiClient()
    .then((client) => client.key.rename(oldName, newName))
}

export function removeKey(name: string): Promise<any> {
  return getApiClient()
    .then((client) => client.key.rm(name))
}

function resolveName(name: string ): Promise<any> {
  return getApiClient()
    .then((client) => client.name.resolve(name, {recursive: true}))
}

/**
 * Publish a hash to IPNS
 *
 * Options {
 *   name: ?string  - defaults to "self"
 *   value: string
 * }
 *
 * @param {Options} options
 */
export function publishName(name: string, value: string): Promise<any> {
  return getApiClient().then((client) => {
    const options = {
      key: name,
      resolve: false,
    }

    return client.name.publish(value, options)
  })
}

/**
 * This function will resolve multiple keys at the same time by mapping them
 * with the resepctive result.
 */
export function resolveKeyValues(list: string[]): Promise<object> {
  const map: any = {}

  const promises = list.map((id) => (
    resolveName(id)
      .then((value) => { map[id] = value })
      .catch(() => { map[id] = "Unresolved" })
  ))

  return Promise.all(promises)
    .then(() => map)
}
