import byteSize from "byte-size"

import {
  CACHE_KEY_DAGS, CACHE_KEY_DIRECTORIES,
  CACHE_KEY_STATS, get as getCache, set as setCache,
} from "../cache"
import { getApiClient } from "./api"

/**
 * Provides using a Promise the stat of an IPFS object. Note: All the Size
 * values are a byteSize object (ex: {value, unit}) to make it human readable
 */
export function getObjectStat(hash: string): Promise <Stat> {
  return new Promise <Stat> ((resolve, reject) => {
    if (!hash) { reject(Error("hash undefined")) }

    // check cache
    getCache(CACHE_KEY_STATS, hash)
    .then((data: Stat) => {
      // if the data is there, resolve it.
      if (data) {
        resolve(data)
        return false
      }
      // if it is not available, process it
      return true
    }).then((shouldProcess: boolean) => {
      if (shouldProcess === false) { return }

      // Get the stat of the hash from the API and cache it
      return getApiClient()
        .then((client) => client.object.stat(hash))
        .then((stat: ApiStat) => {
          const newStat: Stat = {
            BlockSize: byteSize(stat.BlockSize),
            CumulativeSize: byteSize(stat.CumulativeSize),
            DataSize: byteSize(stat.DataSize),
            Hash: stat.Hash,
            LinksSize: byteSize(stat.LinksSize),
            NumLinks: stat.NumLinks,
          }

          // save to cache
          setCache(newStat, CACHE_KEY_STATS, hash)

          resolve(newStat)
        })
    })
  })
}

// isDataDirectory will return a boolean based on the information. It is used
// locally to avoid code duplication.
function isDataDirectory(data: any): boolean {
  return data.length === 2 && data.toString() === "\u0008\u0001"
}

/**
 * isDagDirectory will return a Boolean Promise value based on the content of
 *  the dag: If it contains a IPFS "directory" structure, then returns true
 */
export function isDagDirectory(dag: Dag): Promise <boolean> {
  return new Promise <boolean> ((resolve) => {
    // check cache
    getCache(CACHE_KEY_DIRECTORIES, dag.hash)
    .then((data: boolean) => {
      if (data) {
        resolve(data)
        return false
      }
      // if it is not available, process it
      return true
    }).then((shouldProcess: boolean) => {
      if (shouldProcess === false) { return }

      const result = isDataDirectory(dag.data)

      // save to cache
      setCache(result, CACHE_KEY_DIRECTORIES, dag.hash)

      resolve(result)
    })
  })
}

/**
 * Provides using a Promise the serialized dag of an IPFS object.
 */
export function getObjectDag(hash: string ): Promise <Dag> {
  return new Promise <Dag> ((resolve, reject) => {

  // check cache
    getCache(CACHE_KEY_DAGS, hash).then((data: Dag) => {
      if (data) {
        resolve(data)
        return false
      }
      // if it is not available, process it
      return true
    }).then((shouldProcess: boolean) => {
      if (shouldProcess === false) { return }

      return getApiClient()
        .then((client) => client.object.get(hash))
        .then((dag: ApiDagNode) => {
          const newdag: Dag = dag.toJSON()
          newdag.size = byteSize(dag.size)

          // Perform the links check only if the Dag is a directory
          if (isDataDirectory(dag.data)) {
            newdag.links = dag.links.map((link) => {
              return {
                hash: link.cid.toString(),
                path: link.name,
                size: byteSize(link.size),
              }
            })
          } else {
            newdag.data = "file"
            newdag.links = []
          }

          newdag.hash = hash
          // save to cache
          setCache(newdag, CACHE_KEY_DAGS, hash)
          resolve(newdag)
        })
      })
  })
}
