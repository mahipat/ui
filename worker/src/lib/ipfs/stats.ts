import byteSize from "byte-size"
import { getApiClient } from "./api"

/**
 * Provides the bandwidth stats for the node
 */
export function getBandwidth(): Promise<BandwidthStats> {
  return getApiClient()
    .then((c) => c.stats.bw())
    .then((bw: ApiBandwidthStats) => {
      const stat: BandwidthStats = {
        rateIn: byteSize(Math.ceil((bw.rateIn * 100) / 100)),
        rateOut: byteSize(Math.ceil((bw.rateOut * 100) / 100)),
        totalIn: byteSize(Math.ceil((bw.totalIn * 100) / 100)),
        totalOut: byteSize(Math.ceil((bw.totalOut * 100) / 100)),
      }
      return Promise.resolve(stat)
    })
}
