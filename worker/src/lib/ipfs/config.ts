import { getApiClient } from "./api"

/**
 * `key` is the key of the value that should be fetched from the config file.
 * If no key is passed, then the whole config will be returned.
 */
export function getIPFSConfiguration(key ?: string): Promise<any> {
  return getApiClient()
    .then((client) => client.config.get(key))
}

/**
 *
 * Example:
 * `patchIPFSConfiguration("Discovery.MDNS.Enabled", true)`
 *
 */
export function patchIPFSConfiguration(key: string, value: any): Promise<any> {
  return getApiClient()
    .then((client) => client.config.set(key, value))
    .then(() => getIPFSConfiguration())
}
