import { enhanceObjectList } from "../metadata"
import { getApiClient } from "./api"
/**
 * Fetch the list of pins and enhance the pins to include stat and dag info
 */
export function getPinList(hash: Multihash | null = null): Promise<any[]> {
  return getApiClient()
    .then((client) => {
      if (hash && hash.length > 0) {
        return client.pin.ls(hash, {type: "recursive"})
      } else {
        return client.pin.ls({type: "recursive"})
      }
    })
    .then(enhanceObjectList)
}

/**
 * Provides a Promise that will resolve with true if the hash is pinned
 * or resolve with false otherwise
 */
export function isObjectPinned(hash: string): Promise<boolean> {
  return getApiClient()
    .then((client) => client.pin.ls())
    .then((pins) =>
      // find returns the object, we need to cast it to boolean
      Promise.resolve(!!pins.find((pin: {hash: string}) => pin.hash === hash)),
    )
}
