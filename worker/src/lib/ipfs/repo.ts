import byteSize from "byte-size"
import { getApiClient } from "./api"

/**
 * Provide a promise to get the Repository information. Its RepoSize is actually
 * a byteSize (ex: {value, unit}) to make it human readable
 */
export function getRepoInfo(): Promise<Dag> {
  return getApiClient()
    .then((client) => client.repo.stat({ human: false }))
    .then((stats) => {
      // Providing {value, unit} to the stats.repoSize
      stats.repoSize = byteSize(stats.repoSize)
      // Make these numbers clone-able for postMessage
      stats.numObjects = Number(stats.numObjects)
      stats.storageMax = Number(stats.storageMax)
      return Promise.resolve(stats)
    })
}

/**
 * This process will run the garbage collection
 */
export function runGarbageCollection(): Promise<boolean> {
  return getApiClient()
    .then((client) => client.repo.gc({ quiet: true }))
    .then(() => Promise.resolve(true))
}
