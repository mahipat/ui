import { cleanStore, getStore } from "./datastore"

export const DATASTORE_KEY = "peers"

export function addKnownPeer(peer: KnownPeer): Promise<any> {
  return getStore(DATASTORE_KEY).then((store) => {
    return store.setItem(peer.id, peer)
  })
}

export function deleteKnownPeer(peerId: string): Promise<any> {
  return getStore(DATASTORE_KEY).then((store) => {
    return store.removeItem(peerId)
  })
}

// Return known peers list
export function getKnownPeers(): Promise<KnownPeer[]> {
  return getStore(DATASTORE_KEY).then((s) => {
    return s.keys()
    // For each key get from the store the values
    .then((keys: string[]) => {
      return Promise.all( keys.map((k) => s.getItem(k)) )
    })
  })
}

export function reset(): Promise<any> {
  return cleanStore(DATASTORE_KEY)
}
