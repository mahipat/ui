import localforage from "localforage"

// Return one single localforage instance
export function getStore(name: string ): Promise<any>  {
  localforage.config({ driver: localforage.INDEXEDDB })

  return new Promise((resolve) => {
    resolve(localforage.createInstance({name}))
  })
}

export function cleanStore(name: string ): Promise<any>  {
  // return localforage.dropInstance({name});
  return getStore(name).then((s) => s.clear())
}
