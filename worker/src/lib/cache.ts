import {cleanStore, getStore} from "./datastore"

export const CACHE_KEY_DIRECTORIES = "cache_isDir"
export const CACHE_KEY_STATS = "cache_stats"
export const CACHE_KEY_DAGS = "cache_dags"
export const CACHE_KEY_PROPERTIES = "cache_properties"

// Return a promise with all the caches available
export function getCaches(): Promise<any[]> {
  return Promise.all([
    getStore(CACHE_KEY_DIRECTORIES),
    getStore(CACHE_KEY_STATS),
    getStore(CACHE_KEY_DAGS),
    getStore(CACHE_KEY_PROPERTIES),
  ])
}

/**
 * Clear the cache from memory and persistent storage
 */
export function reset(): Promise<any>  {
  // we must clone the initialCache object, otherwise we will mutate it when we change the CACHE
  return Promise.all([
    cleanStore(CACHE_KEY_DIRECTORIES),
    cleanStore(CACHE_KEY_STATS),
    cleanStore(CACHE_KEY_DAGS),
    cleanStore(CACHE_KEY_PROPERTIES),
  ])
}

export function getCacheSize(): Promise<number> {
  return getCaches()
    .then(([isDir, stats, dag, prop]) => Promise.all([isDir.length(), stats.length(), dag.length(), prop.length()]) )
    .then(([dirL, statsL, dagL, propL]) => dirL + statsL + dagL + propL )
}

// Set a value in a specific cache. Returns a promise
export function set(value: any, cache: string, subkey: string): Promise<any> {
  if (value === undefined || subkey === undefined) { return Promise.resolve() }
  // clone the object and make sure it"s serializable
  const nextValue = JSON.parse(JSON.stringify(value))
  return getStore(cache).then((store) => store.setItem(subkey, nextValue))
}

/**
 * Return a promise to get a specific value from the cache
 */
export function get(cache: string, subkey: string) {
  return getStore(cache)
    .then((store) => store.getItem(subkey))
    .then((object) => {
      if (object) { return object }
      return null
    })
}
