import MatomoTracker from "matomo-tracker"
import uuidv4 from "uuid/v4"
//
import * as settings from "./settings"

const pjson: {version: string, hash: string} = require("../../package.json")
const client = new MatomoTracker(2, "https://siderus.matomo.cloud/piwik.php");
client.on("error", (err) => {
  console.warn("[Matomo] Error", err);
});

/*
 * trackEvent will send a new event with specific data to the current tracking
 * or event system. This helps developing the application by tracking anonymous
 * data from the users behaivour. The user can opt-out or opt-in at any time
 * from the settings or welcome page.
 *
 * It returns a Promise
 */
export function trackEvent(eventName: string , data: any = {}): Promise<any>  {
  if (!navigator.onLine) {
    return Promise.resolve()
  }

  // Check if the user allowed tracking and if the user has an ID already
  return settings.get()
    .then(({ allowUserTracking, statsUserID }) => {
      // If the user did not allow the tracking, block it.
      if (!allowUserTracking) {
        return Promise.resolve()
      }

      const UserID = statsUserID ||  uuidv4()
      if (!statsUserID) {
        settings.patch({ statsUserID: UserID }).then()
    }
      const customData = [
        ["version", `${pjson.version}-${pjson.hash}`],
      ]

      // If the event is `app/started` then count this as a new visit
      const newVisit = eventName === "app/started"

      // Transform data keys into readable matomo objects
      Object.keys(data).forEach((key) => {
        customData.push([key, JSON.stringify(data[key])])
      })

      const eventData = eventName.split("/")

      client.track({
        // If the eventName is its own, then mark it as a "view"
        action_name: eventData[0] === eventName ? "pageview" : eventName,
        cvar: JSON.stringify(customData),
        e_a: eventData[1] || "pageview",
        e_c: eventData[0],
        e_n: eventName,
        new_visit: newVisit,
        ua: `${navigator.userAgent} Orion/${pjson.version}-${pjson.hash}`,
        uid: `${UserID}`,
        url: `https://app.orion.siderus.io/${eventData[0]}`,
      })
    })
    .catch((...errors) => console.log("[Worker] Tracking Error:", errors))
}
