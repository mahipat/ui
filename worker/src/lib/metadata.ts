import { CACHE_KEY_PROPERTIES, get as getCache, set as setCache } from "./cache"
import { getStore } from "./datastore"
import { getObjectDag, getObjectStat, isDagDirectory } from "./ipfs/dags"

export const DATASTORE_KEY_DESCRIPTIONS = "descriptions"

export function describeElement(hash: string, description: string): Promise<any> {
  return getStore(DATASTORE_KEY_DESCRIPTIONS)
    .then((store) => store.setItem(`${hash}"`, description))
}

export function getElementDescription(hash: string): Promise<string> {
  return getStore(DATASTORE_KEY_DESCRIPTIONS)
    .then((store) => store.getItem(`${hash}"`))
    .then((desc) => Promise.resolve(desc || ""))
}

export function getHashProperties(hash: string): Promise<EnhancedObject> {
  return getEnhancedObject(hash)
}

/**
 * Returns a Promise that resolves a fully featured StorageList with more
 * details, ex: Sizes, Links, Hash, Data. Used by the Interface to render the table
 */
export function enhanceObjectList(pins: ApiPin[]): Promise<EnhancedObject[]> {
  // Filter out the indirect objects. Required to reduce API Calls
  pins = pins.filter((pin) => pin.type !== "indirect")

  // Return a promise that will complete when all the data will be
  // available. When done, it will run the main promise success()
  return Promise.all(pins.map((pin) => getEnhancedObject(pin.hash)))
}

/**
 * Provide an enhancedObject from an hash. This method doesn"t use the cache
 */
export function enhanceObject(hash: string): Promise<EnhancedObject> {
  return Promise.all([
    getObjectStat(hash),
    getObjectDag(hash),
    getElementDescription(hash),
  ])
  .then(([stat, dag, description]) =>
    Promise.all([stat, dag, description, isDagDirectory(dag)]),
  )
  .then(([stat, dag, description, isDir]) => {
    const enhObj: EnhancedObject  = {
      dag,
      description,
      hash: stat.Hash,
      isDirectory: isDir,
      stat,
    }
    return Promise.resolve(enhObj)
  })
}

/**
 * Provide an enhancedObject from the cache or calculate it.
 * It will always retrieve the description if it is available as it is the only
 * mutable part of the EnhancedObject
 */
export function getEnhancedObject(hash: string): Promise<EnhancedObject> {
  // Get the Eobj from cache + description
  return getCache(CACHE_KEY_PROPERTIES, hash)
  .then(( eobj: EnhancedObject) => {
    return Promise.all([eobj, getElementDescription(hash)])
  })
  // Update the Description if available
  .then(([eobj, description]) => {
    if (eobj) {
      eobj.description = description
      return Promise.resolve(eobj)
    }

    return enhanceObject(hash)
    .then((neobj) => {
      // Save the object in the cache
      setCache(neobj, CACHE_KEY_PROPERTIES, hash)
      return Promise.resolve(neobj)
    })
  })
}
