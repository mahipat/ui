import { cleanStore, getStore } from "./datastore"

const pjson: {version: string} = require("../../package.json")

const defaultSettings: object = {
  allowUserTracking: true,
  disableWrapping: false,
  enableQuickInfoCards: true,
  gateway: "https://siderus.io",
  ipfsApiAddress: "/ip4/127.0.0.1/tcp/5001",
  language: "en",
  settingsVersion: 1,
  skipGatewayQuery: false,
  spaVersion: pjson.version,
}

export const DATASTORE_KEY = "settings"

/**
 * Return a single value by specifying the key in the settings
 */
export function getKey(key: string): Promise<any> {
  return getStore(DATASTORE_KEY)
    .then((s) => s.getItem(key))
    .then((value) => {
      if (!value) { value = defaultSettings[key] }
      return Promise.resolve(value)
    })
}

/**
 * Return the settings from the persistent storage or the default settings if they don"t exist
 */
export function get(): Promise<any> {
  // copy the settings
  const settings: any = Object.assign({}, {}, defaultSettings)

  return getStore(DATASTORE_KEY).then((s) =>
    s.keys().then((keys: string[]) =>
      // extract the settings values
      Promise.all(keys.map((k: string) =>
        // in a separate promise, populate the values
        s.getItem(k).then((value: string[]) => { settings[k] = value }),
      )),
    )
    // override the default settings with the persisted ones
    .then(() => settings),
  )
}

/**
 *
 * Patch and persist the settings object
 *
 * ```
   Settings {
    settingsVersion: number
    skipGatewayQuery: boolean
  }
  ```
 *
 * @param {Partial<Settings>} newSettings
 */
export function patch(newSettings: any = {}): Promise<any> {
  return Promise.all(Object.keys(newSettings).map((key: string) => {
    const value = newSettings[key]
    return getStore(DATASTORE_KEY).then((s) => s.setItem(key, value))
  }))
}

export function reset() {
  return cleanStore(DATASTORE_KEY)
}
