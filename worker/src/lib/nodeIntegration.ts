import { getApiClient } from "./ipfs/api"
// Note: this file is in javascript becaue we need to inject node integration
// using ../../load-node-modules-into-worker.js and typescript is reporting
// an error when we require path and fs from here.

/**
 * This function allows to save on FS the content of an object to a specific
 * path.
 *
 * See: https://github.com/ipfs/interface-ipfs-core/blob/master/SPEC/FILES.md#getreadablestream
 */
export function saveFileToPath(hash: string, destination: string) {
  if (!hash || !destination) { return Promise.resolve() }

  const { join } = self.node.path
  const { createWriteStream, mkdirSync } = self.node.fs

  return new Promise((resolve, reject) =>
    getApiClient().then((c) => {
      const stream = c.getReadableStream(hash)

      stream.on("data", (file) => {
        const finalDest = join(destination, file.path)

        // First make all the directories
        if (file.type === "dir" || !file.content) {
          try {
            mkdirSync(finalDest)
          } catch (err) {
            if (err.code !== "EEXIST") { throw err }
          }
        } else {
          // Pipe the file content into an actual write stream
          const writeStream = createWriteStream(finalDest)
          file.content.on("data", (data) => {
            writeStream.write(data)
          })
          file.content.resume()
        }
      })

      stream.on("end", resolve)
      stream.on("error", reject)
    }),
  )
}
