interface BandwidthStats {
  rateIn: ByteSize,
  rateOut: ByteSize,
  totalIn: ByteSize,
  totalOut: ByteSize,
}

interface ApiFilesStat {
  type: string,
  blocks: number,
  size: number,
  hash: Multihash,
  cumulativeSize: number,
  withLocality: boolean,
  local: boolean,
  sizeLocal: number
}
