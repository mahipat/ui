/// <reference path="webworker.d.ts" />

interface File {
  path: string
  hash: string
  size: number
  content?: Blob | object | string
}

interface Dag {
  path: string,
  data ?: string | Buffer | any,
  hash: string,
  size: ByteSize,
  links?: Dag[],
  toJSON?: () => any,
}

interface Stat {
  Hash: Multihash,
  NumLinks: number,
  BlockSize: ByteSize,
  LinksSize: ByteSize,
  DataSize: ByteSize,
  CumulativeSize: ByteSize,
}

interface EnhancedObject {
  hash: Multihash,
  stat: Stat,
  isDirectory: boolean,
  dag: Dag,
  description: string,
  type?: string,
}
