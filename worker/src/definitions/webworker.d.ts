
interface WorkerGlobalScope {
  ipfs: any,
  node: any,
}

interface EventMessage {
  method: string,
  id: number,
  value?: any,
  error?: string | object,
}

interface ByteSize {
  value: number | string,
  unit: string,
}
