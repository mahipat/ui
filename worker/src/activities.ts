import "../../utils/sentry"
import * as activities from "./lib/activities"
import * as events from "./lib/events"

import { getApiClient } from "./lib/ipfs/api"

console.log("[Activities Worker] Starting")

// Load node modules such as path,fs,etc. under `self.node`
self.importScripts("load-node-modules-into-worker.js")

console.log("[Activities Worker] Marking Activities as interrupted")
activities.markNotCompletedAsFailed()

if (!navigator.onLine) {
  console.warn("[Activities Worker] No internet connection detected")
}

onmessage = (event: MessageEvent) => {
  const request: EventMessage = event.data
  // console.log("[Worker] Request from client:", request)

  const reply = (responseValue: any) => {
    const message: EventMessage = {
      id: request.id,
      method: request.method,
      value: responseValue,
    }
    // console.log("[Worker] Response to client:", request.method, responseValue)
    postMessage(message)
  }

  const replyWithError = (error: Error) => {
    console.error("[Activities Worker] Error during request:", request.method, error.message)
    const message: EventMessage = {
      error: error.message,
      id: request.id,
      method: request.method,
    }
    postMessage(message)
  }

  switch (request.method) {
    case "ping": {
      return getApiClient().then(() => reply("pong")).catch(replyWithError)
    }
    /**
     * Activities
     */
    case "activities/ls": {
      return activities.get().then(reply).catch(replyWithError)
    }
    case "activities/add": {
      return activities.add(request.value).then(reply).catch(replyWithError)
    }
    case "activities/patch": {
      return activities.patch(request.value).then(reply).catch(replyWithError)
    }
    case "activities/clear": {
      return activities.reset().then(reply).catch(replyWithError)
    }

    case "events/track": {
      return events.trackEvent(request.value.name, request.value.data)
    }

    default: {
      events.trackEvent(request.method)
      return replyWithError(new Error(`Method not supported: ${request.method}`))
    }
  }
}

console.log("[Activities Worker] Ready")
