import "../../utils/sentry"
import * as activities from "./lib/activities"
import * as caches from "./lib/cache"
import * as settings from "./lib/settings"

import { getApiClient } from "./lib/ipfs/api"
import { getIPFSConfiguration, patchIPFSConfiguration } from "./lib/ipfs/config"
import { getRepoInfo, runGarbageCollection } from "./lib/ipfs/repo"

console.log("[Settings Worker] Starting")

// Load node modules such as path,fs,etc. under `self.node`
self.importScripts("load-node-modules-into-worker.js")

console.log("[Settings Worker] Starting to listen for requests")

if (!navigator.onLine) {
  console.warn("[Settings Worker] No internet connection detected")
}

onmessage = (event: MessageEvent) => {
  const request: EventMessage = event.data
  // console.log("[Settings Worker] Request from client:", request)

  const reply = (responseValue: any) => {
    const message: EventMessage = {
      id: request.id,
      method: request.method,
      value: responseValue,
    }
    // console.log("[Settings Worker] Response to client:", request.method, responseValue)
    postMessage(message)
  }

  const replyWithError = (error: Error) => {
    console.error("[Settings Worker] Error during request:", request.method, error.message)
    const message: EventMessage = {
      error: error.message,
      id: request.id,
      method: request.method,
    }
    postMessage(message)
  }

  switch (request.method) {
    case "ping": {
      return getApiClient().then(() => reply("pong")).catch(replyWithError)
    }

    /**
     * Cache
     */
    case "cache/clear": {
      return caches.reset().then(reply).catch(replyWithError)
    }
    case "cache/size": {
      return caches.getCacheSize().then(reply).catch(replyWithError)
    }

    /**
     * Settings and Repository
     */
    case "reset-configuration": {
      return Promise.all([activities.reset(), settings.reset(), caches.reset()])
        .then(reply).catch(replyWithError)
    }
    case "settings/get": {
      return settings.get().then(reply).catch(replyWithError)
    }
    case "settings/getKey": {
      return settings.getKey(request.value).then(reply).catch(replyWithError)
    }
    case "settings/patch": {
      return settings.patch(request.value).then(reply).catch(replyWithError)
    }
    case "repo/get": {
      return getRepoInfo()
        .then(reply).catch(replyWithError)
        .catch(replyWithError)
    }
    case "repo/gc": {
      return runGarbageCollection()
        .then(reply).catch(replyWithError)
        .catch(replyWithError)
    }
    case "config/get": {
      return getIPFSConfiguration(request.value).then(reply).catch(replyWithError)
    }
    case "config/patch": {
      return patchIPFSConfiguration(request.value.key, request.value.value).then(reply).catch(replyWithError)
    }

    default: {
      return replyWithError(new Error(`Method not supported: ${request.method}`))
    }
  }
}

console.log("[Settings Worker] Ready")
