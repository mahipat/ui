import * as Sentry from '@sentry/browser'
import pjson from '../package.json'

Sentry.init({
	dsn: "https://d5a040c26f5447d6ae82c4ab0d1d8f2d@sentry.io/1339056" ,
	environment: pjson.env ? pjson.env : "production",
	release: pjson.version,
})

export default Sentry