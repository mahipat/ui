import i18n from "i18next"
import LanguageDetector from "i18next-browser-languagedetector"

const langda = require("./da.json")
const langde = require("./de.json")
const langen = require("./en.json")
const langes = require("./es.json")
const langfa = require("./fa.json")
const langfr = require("./fr.json")
const langit = require("./it.json")
const langru = require("./ru.json")
const langsv = require("./sv.json")
const langzh = require("./zh.json")

// To add a new language, require it like this:
// const langeo = require("../i18n/eo.json")


i18n
  .use(LanguageDetector)
  .init({
    debug: true,
    fallbackLng: "en",
    resources: {
      da: {translation: langda},
      de: {translation: langde},
      en: {translation: langen},
      es: {translation: langes},
      fa: {translation: langfa},
      fr: {translation: langfr},
      it: {translation: langit},
      ru: {translation: langru},
      sv: {translation: langsv},
      zh: {translation: langzh},
      // To add a new language, specify its configuration like this:
      // eo: {translation: langeo},
    },
  })

export default i18n
