IPFS_HASH_FILE := $(shell mktemp)
GIT_COMMIT ?= $(shell git log -1 --pretty=format:"%h")
GIT_TAG ?= ${shell git tag -l --points-at HEAD | head -n1}

ifeq ($(GIT_TAG),)
	GIT_TAG := v1.0.0-development
endif

# Chunk the v at first char from the git tag
_VERSION := $(shell echo $(GIT_TAG) | sed 's/^\(.\{0\}\)v//g')
# Extract major and minor version, used by subdomains
_MAJMIN_VERSION := $(shell echo "${_VERSION}" | cut -d'.' -f 1,2)
# Extract the channel, if none provided it will equal to _VERSION
_CHANNEL := $(shell echo "${_VERSION}" | sed 's/^.*-//g')
# Defines the subdomain to use for content distribution
SUB_DOMAIN ?=  $(shell echo "${_CHANNEL}" | sed 's/\./-/g' ).

# If we don't have a channel in the version (ex: 1.2.3-beta has beta channel)
ifeq ($(_CHANNEL),$(_VERSION))
	_CHANNEL := production
	SUB_DOMAIN := $(shell echo "${_MAJMIN_VERSION}" | sed 's/\./-/g' ).
endif

# The development channel doesn't exist on subdomains. Use beta instead
ifeq ($(SUB_DOMAIN),development.)
	SUB_DOMAIN := beta.
endif

# Standard across the org
CLOUDFLARE_EMAIL ?= hello@siderus.io
CLOUDFLARE_KEY ?=

# required by cloudflare-cli node module
export CF_API_KEY ?= ${CLOUDFLARE_KEY}
export CF_API_EMAIL ?= ${CLOUDFLARE_EMAIL}
export CF_API_DOMAIN ?= ipfs.rocks

HERA_URL ?= https://api.siderus.io/hera/v0/pin/
HERA_TOKEN ?=

MIXPANEL_RELEASE_TOKEN ?= 76cc5821594f9c83d0b18e8b4aa22ec6

clean:
	rm -rf tmp
	rm -rf dist
	rm -rf public
	rm -rf node_modules
	rm -rf coverage
	rm -rf src/coverage
	$(MAKE) -C worker clean
.PHONY: clean

clean_build:
	rm -rf tmp dist public
	$(MAKE) -C worker clean_build
.PHONY: clean_build

import_web_worker:
	mkdir -p ./dist
	cp -r ./robots.txt dist/
	cp -r ./LICENSE dist/
	cp -r ./README.app.md dist/README.md
	cp -r ./worker/dist/** dist/
	cp -r ./load-node-modules-into-worker.js dist/
.PHONY: import_web_worker

dependencies:
	$(MAKE) -C worker dependencies
	yarn
.PHONY: dependencies

dep: dependencies
.phony: dep

_build_web_worker_dev:
	$(MAKE) -C worker build_dev
	$(MAKE) import_web_worker
.PHONY: _build_web_worker_dev

run: dependencies _build_web_worker_dev
	yarn start
.PHONY: run

_build_web_worker: dependencies
	$(MAKE) -C worker build
	$(MAKE) import_web_worker
.PHONY: _build_web_worker

build: clean_build dependencies _build_web_worker
	yarn build
.PHONY: build

lint: dependencies
	# yarn lint
	yarn lint-ts
	$(MAKE) -C worker lint
.PHONY: lint

test: dependencies
	yarn test
.PHONY: test

# This target will set the version according to the current variable (default TAG)
_prepare_version:
	cat package.json | sed "s/\"1.0.0-development\",$$/\"${_VERSION}\",/g" >> package.new.json
	mv package.new.json package.json
.PHONY: _prepare_version

_prepare_release:
	# Replace environment
	cat package.json | sed "s/\"development\",$$/\"production\",/g" >> package.new.json
	mv package.new.json package.json
	# Replace mixpanel development token
	cat package.json | sed "s/\"9d407c14d888a212cf04c397a95acb7b\",$$/\"${MIXPANEL_RELEASE_TOKEN}\",/g" >> package.new.json
	mv package.new.json package.json
	# Replace commit in use
	cat package.json | sed "s/\"latest-commit\",$$/\"${GIT_COMMIT}\",/g" >> package.new.json
	mv package.new.json package.json
.PHONY: _prepare_release

publish:
	ipfs add -q -r ./dist | tail -n1 >> ${IPFS_HASH_FILE}
	find ./dist/ -type f -exec curl -I -X POST --upload-file {} https://siderus.io/ipfs/ \;
	sleep 5
	curl -H "Authorization: Token ${HERA_TOKEN}" --data "hash=$$(cat ${IPFS_HASH_FILE})&name=Orion-${GIT_TAG}-${GIT_COMMIT}" ${HERA_URL}
	$(MAKE) _prefetch_$$(cat ${IPFS_HASH_FILE})

	# Publish on DNS:
	yarn run cfcli \
		-l 120 -t TXT -e ${CF_API_EMAIL} -d ${CF_API_DOMAIN} edit \
		_dnslink.${SUB_DOMAIN}orion.siderus \
		dnslink=/ipfs/$$(cat ${IPFS_HASH_FILE})

.PHONY: publish

_prefetch_%:
	curl https://meta.siderus.io/ipfs/connect.sh | bash
	sleep 1
	-curl -I -X HEAD https://siderus.io/ipfs/$* --max-time 30
	-curl -I -X HEAD https://ipfs.io/ipfs/$* --max-time 30

i18n_%:
	$(MAKE) -C i18n $*