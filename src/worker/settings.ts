import uuidv4 from "uuid/v4"
import { eventTrack, startActivity, stopActivity } from "./activities"
import { onWorkerMessage, requestWorker, waitForWorker } from "./index"

const worker: Worker | any = typeof Worker !== "undefined" ? new Worker("settings.js") : {}
worker.onmessage = onWorkerMessage

export function pingWorker() {
  return requestWorker(worker, { method: "ping" })
}

/**
 * Return the settings from the persistent storage or the default settings if they don't exist
 */
export function getSettings() {
  return requestWorker(worker, { method: "settings/get" })
}

/**
 *
 * Patch and persist the settings object
 *
  ```
   Settings {
    settingsVersion: number
    skipGatewayQuery: boolean
  }
  ```
 *
 * @param {Partial<Settings>} newSettings
 */
export function patchSettings(nextSettings: object) {
  return requestWorker(worker, { method: "settings/patch", value: nextSettings })
}

/**
 *
 * Get a single Setting Value by key
 * @param {string} key
 * @returns {string} value
 */
export function getSettingByKey(key: string) {
  return requestWorker(worker, { method: "settings/getKey", value: key })
}

/**
 * Return the IPFS node configuration, or a specific value if key is specified.
 *
 * @param {?string} key
 */
export function getIPFSConfig(key: string = "") {
  return requestWorker(worker, { method: "config/get", value: key })
}

/**
 * Add or replace a key in the IPFS node configuration.
 *
 * @param {string} key
 * @param {*} value
 */
export function patchIPFSConfig(key: string, value: any) {
  return requestWorker(worker, { method: "config/patch", value: { key, value } })
}

/**
 * Provides a Promise that will resolve the repository info (size etc..)
 */
export function getRepo() {
  return requestWorker(worker, { method: "repo/get" })
}

/**
 * Provides a Promise that will instruct the daemon to run the GC process
 * and remove content not pinned.
 */
export function runGarbageCollection() {
  const activityId = uuidv4()
  startActivity(activityId, "repo/gc", `Running garbage collector...`, "/settings")
  eventTrack("repo/gc")
  return requestWorker(worker, { method: "repo/gc" })
    .then((anything) => {
      stopActivity(activityId)
      return Promise.resolve(anything)
    })
}

export function resetConfiguration() {
  eventTrack("settings/reset")
  return requestWorker(worker, { method: "reset-configuration" })
}

export function clearCache() {
  eventTrack("settings/clearCache")
  return requestWorker(worker, { method: "cache/clear" })
}

export function getCacheSize() {
  return requestWorker(worker, { method: "cache/size" })
}

export function wait() {
  return waitForWorker(worker, "settings")
}
