/// <reference path="../../worker/src/definitions/api.d.ts" />

import uuidv4 from "uuid/v4"
import { eventTrack, startActivity, stopActivity } from "./activities"
import { onWorkerMessage, requestWorker, waitForWorker } from "./index"

const worker: Worker | any = typeof Worker !== "undefined" ? new Worker("keys.js") : {}
worker.onmessage = onWorkerMessage

export function pingWorker() {
  return requestWorker(worker, { method: "ping" })
}

export function getKeyList() {
  return requestWorker(worker, { method: "keys/ls" })
}

export function addKey(name: string) {
  eventTrack("keys/add")
  return requestWorker(worker, { method: "keys/add", value: name })
}

export function removeKey(name: string) {
  eventTrack("keys/rm")

  if (name === "self") {
    return Promise.reject("Impossible to remove the special 'self' IPNS key")
  }

  return requestWorker(worker, { method: "keys/rm", value: name })
}

export function renameKey(oldName: string, newName: string) {
  eventTrack("keys/rename")

  if (oldName === "self") {
    return Promise.reject("Impossible to rename the special 'self' IPNS key")
  }

  return requestWorker(worker, {
    method: "keys/rename",
    value: { newName, oldName },
  })
}

export function publishIPNS(name: string, value: string) {
  const activityId = uuidv4()
  startActivity(activityId, "keys/publish", `Updating ${name} to ${value}`, "/names")
  eventTrack("keys/publish")
  return requestWorker(worker, {
      method: "ipns/publish",
      value: { name, value },
    }, 0)
    .then((anything) => {
      stopActivity(activityId)
      return Promise.resolve(anything)
    })
    .catch((error) => {
      stopActivity(activityId, "failed", "")
      return Promise.reject(error)
    })
}

export function resolveKeyValues(ids: string[]) {
  // As this might take more than 60 seconds, disable timeout
  return requestWorker(worker, { method: "keys/values", value: ids }, 0)
}

export function wait() {
  return waitForWorker(worker, "keys")
}
