/// <reference path="../../worker/src/definitions/activities.d.ts" />

import { onWorkerMessage, requestWorker, sendWorker, waitForWorker } from "./index"

const worker: Worker | any = typeof Worker !== "undefined" ? new Worker("activities.js") : {}
worker.onmessage = onWorkerMessage

export function pingWorker() {
  return requestWorker(worker, { method: "ping" })
}

export function clearActivities() {
  return requestWorker(worker, { method: "activities/clear" })
}

export function addActivity(activity: Activity) {
  return requestWorker(worker, { method: "activities/add", value: activity })
}

export function patchActivity(activity: object) {
  return requestWorker(worker, { method: "activities/patch", value: activity })
}

export function getActivityList(): Promise<Activity[]> {
  return requestWorker(worker, { method: "activities/ls" }, 0)
}

export function eventTrack(eventName: string, data = {}) {
  return sendWorker(worker, {
    method: "events/track",
    value: { name: eventName, data },
  })
}

export function wait() {
  return waitForWorker(worker, "activities")
}

/**
 * Custom tools for activities
 * */

export function startActivity(id: string, type: string, name: string, link = ""): string {
  const newAct: Activity = {
    link,
    name,
    progress: 0,
    status: "in progress",
    type,
    uuid: id,
  }

  if (link) {
    newAct.link = link
  }

  addActivity(newAct)
  return id
}

export function updateProgress(id: string, progress: number): string {
  patchActivity({
    progress,
    uuid: id,
  })
  return id
}

export function stopActivity(id: string, status = "completed", link = ""): string {
  const patch: any = {
    progress: 1,
    status,
    uuid: id,
  }

  if (link) {
    patch.link = link
  }

  patchActivity(patch)
  return id
}
