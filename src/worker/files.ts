/// <reference path="../../worker/src/definitions/object.d.ts" />
/// <reference path="../../worker/src/definitions/stats.d.ts" />
/// <reference path="../../worker/src/definitions/api.d.ts" />

import uuidv4 from "uuid/v4"
import { readAsBuffer } from "../../utils/readAsBuffer"
import { askForSavingLocation } from "../electron/dialogs"

import { eventTrack, startActivity, stopActivity, updateProgress } from "./activities"
import { onWorkerMessage, requestWorker, waitForWorker } from "./index"

const worker: Worker | any = typeof Worker !== "undefined" ? new Worker("files.js") : {}
worker.onmessage = onWorkerMessage

export function pingWorker() {
  return requestWorker(worker, { method: "ping" })
}

export function getPinList(hash: Multihash = ""): Promise<EnhancedObject[]> {
  // Max timeout is big due to big timing for lot of content in slow go-ipfs
  return requestWorker(worker, { method: "pin/ls", value: hash }, 10 * 60 * 1000)
}

export function describeElement(hash: Multihash, description: string) {
  return requestWorker(worker, {
    method: "file/describe",
    value: { hash, description },
  })
}

export function getHashProperties(hash: Multihash): Promise<EnhancedObject> {
  return requestWorker(worker, { method: "file/properties", value: hash })
}

/**
 * Ask the user for a download destination using the native open dialog from electron
 * and then pass the hash and the path to the worker to perform a download using streams.
 *
 * @namespace electron
 */
export function downloadFile(hash: Multihash) {
  eventTrack("file/download")
  return requestWorker(worker, {
    method: "file/download",
    value: {
      hash,
      path: askForSavingLocation(),
    },
  })
}

export function removeFiles(hash: Multihash | Multihash[]) {
  eventTrack("file/remove")
  return requestWorker(worker, { method: "file/remove", value: hash })
}

/**
 * This is used only when adding from a browser. The flow is different when
 * using the Orion electron shell.
 * https://developer.mozilla.org/en-US/docs/Web/API/File
 *
 * File {
 *   name: string
 * }
 *
 * @param {File} file
 */
export function addFile(file: any) {
  eventTrack("file/add")
  return readAsBuffer(file)
    .then((buf) => requestWorker(worker, { method: "file/add", value: buf }))
}

export function importFile(hash: Multihash) {
  const activityId = uuidv4()
  startActivity(activityId, "file/import", `Import: ${hash}`, `/properties/${hash}`)
  eventTrack("file/import")

  /**
   * WARNING: WORKAROUND AHEAD. PLEASE DO NOT CONSIDER THIS AS SOMETHIGN TO COPY
   */
  // Set a new interval to check the percentage of the import and report it
  // as an activity update. (Loading Bar).
  let checkInProgress = false
  const intId = setInterval(() => {
    if (checkInProgress) {
      return
    }
    checkInProgress = true

    mfsFilesStat(`/ipfs/${hash}`).then((res: any) => {
      const perc = res.sizeLocal / res.cumulativeSize
      updateProgress(activityId, perc)
      checkInProgress = false
    })
    .catch((err) => {
      checkInProgress = false
      console.warn("[Files] error checking file", err)
    })
  }, 1 * 1500)

  return requestWorker(worker, { method: "file/import", value: hash }, 0)
    .then( (anything) => {
      clearInterval(intId)
      stopActivity(activityId)
      return Promise.resolve(anything)
    })
    .catch((error) => {
      clearInterval(intId)
      stopActivity(activityId, "failed", "")
      return Promise.reject(error)
    })

}

export function getFilePeers(hash: Multihash) {
  eventTrack("file/peers")
  return requestWorker(worker, { method: "file/peers", value: hash })
}

export function getFileStat(hash: Multihash): Promise<Stat> {
  eventTrack("file/stat")
  return requestWorker(worker, { method: "file/stat", value: hash })
}

/**
 * Provides the MFS files/stat value. Used to know how much data we have stored
 * locally of a specific hash that we are imported. Warning: used for workarouds
 * Potential spagetty code.
 *
 * @param ipfsPath String containing the mfs path or ipfs path (ex /ipfs/Qm1...)
 */
export function mfsFilesStat(ipfsPath: string) {
  return requestWorker(worker, { method: "file/mfs/stat", value: ipfsPath }, 0)
}

export function wait() {
  return waitForWorker(worker, "files")
}
