interface EventMessage {
  method: string,
  id: number,
  value?: any,
  error?: string | object,
}

interface EventRequest {
  method: string,
  value?: any,
  id?: number,
}
