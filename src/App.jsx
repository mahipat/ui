/* eslint import/no-named-as-default: 0 */
import * as Sentry from '@sentry/browser'
import React from 'react'
import { observer, Provider as StoreProvider } from 'mobx-react'
import { HashRouter, Route } from 'react-router-dom'
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import { SnackbarProvider } from 'notistack'
import 'typeface-roboto'

import i18n from '../i18n
//
import AppBar from './components/AppBar'
import AppMenu from './components/AppMenu'
import LoadingOverlay from './components/LoadingOverlay'
import ShellRouter from './electron/ShellRouter'

// Workers
import {
  wait as waitForSettings, patchSettings, clearCache
} from './worker/settings'
import {
  wait as waitForConnectivity, connectToSiderus
} from './worker/connectivity'
import {
  wait as waitForActivities, eventTrack
} from './worker/activities'
import { wait as waitForKeys } from './worker/keys'
import { wait as waitForFiles } from './worker/files'

// Custom Theme
import theme from './theme'

// Dialogs
import WelcomeDialog from './components/Welcome'
import ErrorDialog from './components/ErrorDialog'

// Containers
import FilesView from './containers/FilesView'
import ActivitiesView from './containers/ActivitiesView'
import KeysView from './containers/KeysView'
import SettingsView from './containers/Settings'
import PropertiesView from './containers/PropertiesView'
import NetworkView from './containers/NetworkView'

// Stores (Mobx)
import ActivityStore from './stores/activities'
import FilesStore from './stores/files'
import KeysStore from './stores/keys'
import NetworkStore from './stores/networking'
import SettingsStore from './stores/settings'

import pjson from '../package.json'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@observer
class App extends React.Component {
  state = {
    loadingText: '',
    menuOpen: false,
    fatalError: '',
    workerStarted: false,
  }

  handleCloseMenu = () => {
    this.setState({ menuOpen: false })
  }

  handleToggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  // Wait for the worker, get the settings, check if we need to clean the
  // cache and get the repo to test the CORS / Daemon
  componentWillMount () {
    if(!navigator.onLine) {
      console.warn('[SPA][App] No internet connection detected')
    }

    // Check if the browser supports  Worker
    if(typeof Worker === "undefined") {
      console.error('[SPA][App] Web Worker not supported')
      const workerError =  new Error(`The browser doesn't support Web Workers. Please use a Modern browser, Mozilla Firefox is suggested`)
      this.setState({ fatalError: workerError })
      return
    }

    this.setState({loadingText: i18n.t('status_AppStarting')})
    // Wait for all the workers
    return new Promise((resolve) => setTimeout(() => resolve(), 250))
    .then(() => Promise.all([
      waitForSettings(), waitForFiles(), waitForKeys(),
      waitForActivities(), waitForConnectivity()
    ])).then(() => {
      console.debug('[SPA][App] Workers ready')
      eventTrack('app/started', { isElectron })
      this.setState({ workerStarted: true})
      // seel 0.25 secs
      return new Promise((resolve) => setTimeout(() => resolve(), 250))
    })
    .then(() => {
      this.setState({loadingText: i18n.t('status_AppFetchingSettings')})
      // Start loops for Activities and fetch the app settings
      ActivityStore.startLoop()
      return SettingsStore.fetchAppSettings()
    })
    .then(() => {
      console.debug('[SPA][App] Settings Fetched')
      // If the previous version is not maching, clear the cache
      if(SettingsStore.settings.spaVersion !== pjson.version) {
        this.setState({loadingText: i18n.t('status_AppClearCache')})
        console.log('[SPA][App] Cleaning the cache')
        return clearCache()
          // Once cleared, patch the version
          .then(() => patchSettings({spaVersion: pjson.version}))
      }

      this.setState({loadingText: i18n.t('status_AppConnecting')})
      // Start a connection but if it fails continue
      connectToSiderus().then(() => {
        console.log('[SPA][App] Connected to Siderus Peers')
      }).catch((err) => {
        console.warn(`[SPA] Unable to connect to Siderus Peers: ${err}`)
      })

      this.setState({loadingText: i18n.t('status_AppInitialFetch')})
    })
    // Once cache cleared set the settings
    // Fetch data in the Stores for ininitial data.
    .then(() => Promise.all([
      SettingsStore.fetch(),
      FilesStore.fetch(),
    ]))
    // Now "lower" priority stores
    .then(() => Promise.all([ NetworkStore.fetch(), KeysStore.fetch() ]))
    .then(() => {
      console.debug('[SPA][App] Initial fetching completed')
    })
    .catch(error => {
      this.setState({ fatalError: error })
      console.error('[SPA][App]', error)
    })
  }

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo)
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key])
      })
      Sentry.captureException(error)
    })
    // Report the error but don't show it if it is  continously appearing
    if (this.state.fatalError !== '') return
    this.setState({ fatalError: error })
  }

  handleWelcomeFinish = (allowUserTracking) => {
    patchSettings({
      allowUserTracking,
      welcomeVersionSeen: pjson.welcomeVersion
    })
  }

  render () {
    const { menuOpen, fatalError, workerStarted, loadingText } = this.state
    const { classes } = this.props

    const isLoading = FilesStore.loading || SettingsStore.loading

    return (
      <StoreProvider
        SettingsStore={SettingsStore}
        FilesStore={FilesStore}
        ActivityStore={ActivityStore}
        KeysStore={KeysStore}
        NetworkStore={NetworkStore}
      >
      <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        autoHideDuration={2000}
        maxSnack={4}
        anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
      >
      <React.Fragment>
        {
          fatalError &&
          <ErrorDialog ipfsApiAddress={SettingsStore.settings.ipfsApiAddress} error={fatalError} />
        }
        {/*
          Load the welcome window only if there is no error and if the settings
          have been loaded (settings.ipfsApiAddress !== undefined)
        */}
        {
          !fatalError && !isLoading && SettingsStore.settings.ipfsApiAddress &&
            <WelcomeDialog onFinish={this.handleWelcomeFinish} />
        }
        <AppBar onToggleMenu={this.handleToggleMenu} />
        <div className={classes.root}>
          <CssBaseline />
          <HashRouter>
              <LoadingOverlay loading={isLoading && !fatalError} subtext={loadingText}>
                <AppMenu open={menuOpen && workerStarted} onCloseMenu={this.handleCloseMenu} />
                <ShellRouter />
                { !isLoading &&
                  <div className={classes.content}>
                    <Route exact path="/" component={FilesView} />
                    <Route exact path="/names" component={KeysView} />
                    <Route exact path="/activities" component={ActivitiesView} />
                    <Route exact path="/settings" component={SettingsView} />
                    <Route exact path="/network" component={NetworkView} />
                    <Route path="/properties/:cid" component={PropertiesView} />
                  </div>
                }
              </LoadingOverlay>
          </HashRouter>
        </div>
      </React.Fragment>
      </SnackbarProvider>
      </MuiThemeProvider>
      </StoreProvider>
    )
  }
}

const styles = {
  root: {
    display: 'flex',
    // set height to 100% to allow drag and drop
    height: 'calc(100vh)',
    overflow: 'hidden'
  },
  content: {
    marginTop: 48,
    padding: 12,
    overflowY: 'auto',
    flexGrow: 1
  }
}

export default withStyles(styles)(App)