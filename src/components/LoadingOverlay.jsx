import React from 'react'
import { withStyles } from '@material-ui/core/styles'
// import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Typography } from '@material-ui/core'

import i18n from '../../i18n'


const LoadingOverlay = ({ children, classes, loading, text, subtext }) => (
  <React.Fragment>
    <Dialog
      open={loading} className={classes.dialog}
      aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description"
      disableEscapeKeyDown disableBackdropClick
    >
      <DialogTitle className={classes.title} id="alert-dialog-title">{ text || i18n.t("status_loading") }</DialogTitle>
      <DialogContent className={classes.content}>
        <CircularProgress />
      </DialogContent>
      { subtext &&
        <Typography variant='subtitle1' className={classes.subtext}>
          {subtext}
        </Typography>
      }
    </Dialog>
    {children}
  </React.Fragment>
)

const styles = {
  dialog: {
    'z-index': 998,
  },
  title: {
    textAlign: 'center',
  },
  subtext: {
    textAlign: 'center',
    padding: '0 20px 10px'
  },
  content: {
    textAlign: 'center',
    minWidth: '200px',
  }
}

export default withStyles(styles)(LoadingOverlay)
