import React from 'react'
import { withRouter } from 'react-router'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import TextField from '@material-ui/core/TextField'
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck'
import Typography from '@material-ui/core/Typography'
import mafmt from 'mafmt'

import { LineChart, Line, YAxis, XAxis, Tooltip } from 'recharts'
import { pingPeer } from '../../worker/connectivity'

import i18n from '../../../i18n'

const CONNECT_PLACEHOLDER_TEXT = "/ip4/127.0.0.1/ipfs/Qm12342..."

const styles = theme => ({
  resultsText: {
    paddingBottom: theme.spacing.unit * 2,
  },
  content: {
    minWidth: 500,
  },
  centered: {
    textAlign: 'center',
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withRouter
@withStyles(styles)
@inject('NetworkStore')
@observer
class PingDialog extends React.Component {
  state = {
    loading: false,
    isValid: true,
    results: [],
    error: '',
  }

  handleOpen = () => {
    this.setState({
      results: [], loading: false, isValid: true, error: '',
    })
    this.props.NetworkStore.pingDialogAddress = ''
    this.props.NetworkStore.isPingDialogOpen = true
  }

  handleClose = () => {
    this.setState({error: '', results: []})
    this.props.NetworkStore.pingDialogAddress = ''
    this.props.NetworkStore.isPingDialogOpen = false
  }

  startPing = () => {
    const address = this.props.NetworkStore.pingDialogAddress

    let isValid = false
    try {
      isValid = mafmt.IPFS.matches(address)
    } catch (err) {
      isValid = false
    }

    if(!isValid) {
      return this.setState({isValid})
    }

    // Set loading state
    this.setState({loading: true, results: []})
    return pingPeer(address, 10).then((data) => {
      const results = data.map((el) => {
        if (!el.time) return el
        const newTime = (el.time / 1000000).toFixed(2)
        el.time = newTime
        return el
      })
      this.setState({results, loading: false})
    }).catch((error) => {
      this.setState({results: [], loading: false, error})
    })

  }

  render () {
    const { classes, NetworkStore } = this.props
    const { loading, isValid, results, error } = this.state

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <NetworkCheckIcon className={classes.leftIcon} />
            {i18n.t("dialog_pingButton")}
        </Button>
        <Dialog
          open={NetworkStore.isPingDialogOpen}
          onClose={this.handleClose}
          disableEscapeKeyDown={!!loading}
          disableBackdropClick={!!loading}
        >
          { results.length === 0 && !loading && !error && <DialogTitle>{i18n.t("dialog_pingPeer_title")}</DialogTitle> }
          { results.length !== 0 && !loading && !error && <DialogTitle>{i18n.t("dialog_pingPeer_successTitle")}</DialogTitle> }
          { !loading && error && <DialogTitle>{i18n.t("dialog_pingPeer_failedTitle")}</DialogTitle> }
          <DialogContent className={classes.content}>
          { /* When not loading and no results */}
          { results.length === 0 && !loading && !error &&
            <React.Fragment>
              <DialogContentText>
                {i18n.t("dialog_pingPeer_text")}
              </DialogContentText>
              <List>
                <ListItem>
                  <ListItemText>
                    <TextField
                      fullWidth
                      label={i18n.t("network_peeraddress")}
                      placeholder={CONNECT_PLACEHOLDER_TEXT}
                      value={NetworkStore.pingDialogAddress}
                      onChange={e => {NetworkStore.pingDialogAddress = e.target.value}}
                      error={!isValid}
                      margin='none'
                    />
                  </ListItemText>
                </ListItem>
              </List>
            </React.Fragment>
          }
          { /* When loading */}
          { loading &&
            <div className={classes.centered}>
              <CircularProgress />
              <Typography>{i18n.t("dialog_pingPeer_inProgress")}</Typography>
            </div>
          }
          { !loading && results.length !== 0 && !error &&
            <React.Fragment>
              <DialogContentText className={classes.resultsText}>
                {i18n.t("dialog_pingPeer_successText")}
              </DialogContentText>
              <Grid container direction="column" justify="center">
                <Grid item xs={12}>
                  <LineChart width={500} height={250} data={results}>
                    <Line type="monotone" name="Latency" unit="ms" dataKey="time" stroke="#8884d8" />
                    <XAxis label="sequence" hide />
                    <YAxis unit="ms" />
                    <Tooltip/>
                  </LineChart>
                </Grid>
              </Grid>
            </React.Fragment>
          }
          { !loading && error &&
            <React.Fragment>
              <DialogContentText className={classes.resultsText}>
                {i18n.t("dialog_pingPeer_failedText")}
                <br />
                <br />
                {error.toString()}
              </DialogContentText>
            </React.Fragment>
          }
          </DialogContent>
          <DialogActions>
            { !loading &&
            <Button onClick={this.handleClose}>
              {i18n.t("dialog_cancelButton")}
            </Button>
            }
            { !loading && results.length === 0 && !error &&
            <Button onClick={this.startPing}>
              {i18n.t("dialog_pingButton")}
            </Button>
            }
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}


export default PingDialog