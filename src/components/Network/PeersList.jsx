import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
//
import i18n from '../../../i18n'

const hideOverflow = {
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
}

const styles = theme => ({
  root: {
    overflowX: 'auto'
  },
  hint: {
    padding: '20px',
  },
  highlight: {
    display: 'flex',
    justifyContent: 'space-between',
    // theme.palette.type is either 'light' or 'dark'
    backgroundColor: theme.palette.secondary[theme.palette.type]
  },
  table: {
    tableLayout: 'fixed',

    '& th:nth-child(1)': { width: '30%', ...hideOverflow },
    '& th:nth-child(2)': { width: '50%', ...hideOverflow },
    '& th:nth-child(3)': { width: '10%', ...hideOverflow },
    '& th:nth-child(4)': { width: 68 },

    '& td:nth-child(2)': hideOverflow,
  }
})

@withStyles(styles)
class PeersList extends React.Component {
  render () {
    const { classes, children } = this.props

    return (
      <div className={classes.root}>
        <Toolbar>
          <Typography variant='h6'>
          {i18n.t("network_list_title")}
          </Typography>
        </Toolbar>
        <Table className={classes.table} padding='checkbox'>
          <TableHead>
            <TableRow>
              <TableCell>{i18n.t("network_list_header_name")}</TableCell>
              <TableCell>{i18n.t("network_list_header_address")}</TableCell>
              <TableCell align="right">{i18n.t("network_list_header_status")}</TableCell>
              <TableCell align="right">{i18n.t("network_list_header_actions")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { children }
          </TableBody>
        </Table>
        { React.Children.count(children) <= 0 &&
          <Typography className={classes.hint}>
            {i18n.t("network_list_emptyHelp")}
          </Typography>
        }
      </div>
    )
  }
}

export default (PeersList)
