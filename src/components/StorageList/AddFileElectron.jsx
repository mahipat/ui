import React from 'react'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder'
import { withStyles } from '@material-ui/core/styles'
import { askForFilePaths, askForDirectoryPaths } from '../../electron/dialogs'
//
import i18n from '../../../i18n'

const ADD_DIR_BUTTON_LABEL = 'Add directories'

/**
 * AddFileElectron is composed of two buttons: `Add files` and `Add directories`
 * (When running the app within `electron` we also support directory upload)
 *
 * Each button, when clicked, will call the appropriate electron dialog asking the user
 * to select a number of files or directories he/she wishes to upload.
 *
 * When the user has selected the file(s) to upload, `props.onFilesChange`
 * will be called with the list of paths. (e.g. `["/home/alice/Desktop/cat.gif"]`)
 *
 * AddFileProps {
 *   onFilesChange: (paths: Array<string>) => void
 * }
 *
 * @namespace electron
 * @param {AddFileProps} props
 */
const AddFileElectron = ({ classes, onFilesChange }) => {
  const handleAddFile = () => onFilesChange(askForFilePaths())
  const handleAddDir = () => onFilesChange(askForDirectoryPaths())

  return (
    <React.Fragment>
      <Button onClick={handleAddFile}>
        <AddIcon className={classes.leftIcon} />
        {i18n.t("storage_addFilesButton")}
      </Button>
      <Button onClick={handleAddDir}>
        <CreateNewFolderIcon className={classes.leftIcon} />
        {i18n.t("storage_addDirectoryButton")}
      </Button>
    </React.Fragment>
  )
}

const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

export default withStyles(styles)(AddFileElectron)
