import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'

import i18n from '../../../i18n'

class DescribeElementDialog extends React.Component {
  state = {
    newDescription: false
  }

  handleSubmit = () => {
    const { cid, onSubmit, onClose } = this.props
    const { newDescription } = this.state

    // if no editing has been done just close the modal
    if (newDescription === false) {
      return onClose()
    }

    onSubmit(cid, newDescription)
    this.setState({ newDescription: false })
  }

  handleDescriptionChange = (event) => {
    this.setState({ newDescription: event.target.value })
  }

  render () {
    const { newDescription } = this.state
    const { classes, cid, description, open, onClose } = this.props

    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{i18n.t("dialog_describeElement_title")}</DialogTitle>
        <DialogContent className={classes.content}>
          <TextField
            label={i18n.t("dialog_describeElement_nameLabel")}
            margin='normal'
            fullWidth
            value={cid || ''}
            disabled
          />
          <TextField
            label={i18n.t("dialog_describeElement_descriptionLabel")}
            margin='normal'
            fullWidth
            multiline
            // if description is null or undefined pass an empty string
            value={newDescription === false ? (description || '') : newDescription}
            onChange={this.handleDescriptionChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
            {i18n.t("dialog_cancelButton")}
          </Button>
          <Button
            onClick={this.handleSubmit}
            color="primary"
            variant='outlined'
          >
            {i18n.t("dialog_importButton")}
        </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

const styles = {
  content: {
    minWidth: 500
  }
}

export default withStyles(styles)(DescribeElementDialog)
