import React from 'react'
import { inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import FileIcon from '@material-ui/icons/InsertDriveFile'
import FolderIcon from '@material-ui/icons/Folder'
import DeleteIcon from '@material-ui/icons/Delete'
import InfoIcon from '@material-ui/icons/Info'
import SaveAltIcon from '@material-ui/icons/SaveAlt'
import LinkIcon from '@material-ui/icons/Link'
import LabelIcon from '@material-ui/icons/Label'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import Divider from '@material-ui/core/Divider'
import Hidden from '@material-ui/core/Hidden'

import OpenInBrowser from '../MenuElements/OpenInBrowser'
import CopyButton from '../MenuElements/Copy'
import InternalLink from '../MenuElements/InternalLink'

import i18n from '../../../i18n'

const ACTION_MENU_ITEMS = [
  { handlerName: 'onDescribe', label: i18n.t("menu_describe"), icon: <LabelIcon /> },
  { handlerName: 'onDownload', label: i18n.t("menu_download"), icon: <SaveAltIcon />, hidden: () => !window.electron },
  { handlerName: 'onPublish', label: i18n.t("menu_publishToIPNS"), icon: <LinkIcon /> },
]

const styles = {
  clickableCell: {
    cursor: 'pointer'
  }
}

@withRouter
@withStyles(styles)
@inject('SettingsStore')
class StorageElement extends React.Component {
  state = {
    actionMenuAnchor: null
  }

  handleActionMenuOpen = (event) => {
    this.setState({ actionMenuAnchor: event.currentTarget })
  }

  handleActionMenuClose = () => {
    this.setState({ actionMenuAnchor: null })
  }

  render () {
    const { actionMenuAnchor } = this.state
    const {
      classes,
      hash, dag, motherHash, stat, isDirectory, size, path, description,
      isSelected, isPinned = true, onSelect, history, SettingsStore
    } = this.props

    const gateway = SettingsStore.settings.gateway || "https://siderus.io"

    const ClickableCell = props => (
      <TableCell
        className={classes.clickableCell}
        onDoubleClick={() => { history.push(`/properties/${hash}`) }}
        {...props}
      />
    )

    let fileNames
    if(dag && dag.links) {
      fileNames = dag.links
        // filter out unnamed links (usually pieces of a file)
        .filter(link => !!link.path)
        .map(link => link.path)
        .join(', ')
    }

    let cumulativeSize
    if(stat && stat.CumulativeSize)
      cumulativeSize = stat.CumulativeSize
    else
      cumulativeSize = size

      return (
      <TableRow
        hover
        role='checkbox'
        aria-checked={isSelected}
        selected={isSelected}
        forhash={hash}
      >
        <TableCell onClick={() => { onSelect(hash) }} className={classes.clickableCell}>
          <Checkbox checked={isSelected} />
        </TableCell>
        <ClickableCell>
          {
            isDirectory
              ? <FolderIcon />
              : <FileIcon />
          }
        </ClickableCell>
        <ClickableCell>
          {path || fileNames || (isDirectory ? i18n.t("empty") : i18n.t("unknown"))}
        </ClickableCell>
        <ClickableCell>
          {description || ' '}
        </ClickableCell>
        <Hidden xsDown>
          <ClickableCell component='th' scope='row'>
            {hash}
          </ClickableCell>
        </Hidden>
        <Hidden xsDown>
          <ClickableCell align='right'>
            {cumulativeSize ? `${cumulativeSize.value} ${cumulativeSize.unit}` : 'Unknown'}
          </ClickableCell>
        </Hidden>
        <TableCell align='right'>
          <IconButton
            aria-label='Actions'
            aria-owns={actionMenuAnchor ? 'action-menu' : null}
            aria-haspopup='true'
            onClick={this.handleActionMenuOpen}
          >
            <MoreHorizIcon />
          </IconButton>
          <Menu
            id='action-menu'
            anchorEl={actionMenuAnchor}
            open={Boolean(actionMenuAnchor)}
            onClose={this.handleActionMenuClose}
          >
            <OpenInBrowser key={0} to={`${gateway}/ipfs/${hash}`} />
            <CopyButton key={1} value={hash} />
            <CopyButton key={2} value={motherHash ? `${gateway}/ipfs/${motherHash}/${path}` : `${gateway}/ipfs/${hash}`} label={i18n.t("menu_copyPublicLink")} />
            <Divider key={3} />
            { isPinned &&
              <MenuItem key={4} handler='onRemove'>
                <ListItemIcon>
                  <DeleteIcon />
                </ListItemIcon>
                <ListItemText
                  inset
                  primary={i18n.t("menu_removePin")}
                  onClick={() => {
                    this.handleActionMenuClose()
                    this.props.onRemove(hash)
                  }}
                />
              </MenuItem>
            }
            {
              ACTION_MENU_ITEMS.map(({ label, icon, handlerName, hidden }, index) => {
                if (hidden && hidden())
                  return null

                if (label === null)
                  return <Divider key={index+5} />

                return (
                  <MenuItem key={index+5} handler={handlerName}>
                    <ListItemIcon>
                      {icon}
                    </ListItemIcon>
                    <ListItemText
                      inset
                      primary={label}
                      onClick={() => {
                        this.handleActionMenuClose()
                        const handler = this.props[handlerName]
                        handler(hash)
                      }}
                    />
                  </MenuItem>
                )
              })
            }
            <Divider key={ACTION_MENU_ITEMS.length+7} />
            <InternalLink
              iconComponent={<InfoIcon />} key={ACTION_MENU_ITEMS.length+5}
              to={`/properties/${hash}`} label={i18n.t("menu_propertiesPin")}
            />
          </Menu>
        </TableCell>
      </TableRow>
    )
  }

}

export default StorageElement
