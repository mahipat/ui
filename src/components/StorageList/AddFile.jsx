import React from 'react'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import { withStyles } from '@material-ui/core/styles'
//
import i18n from '../../../i18n'


/**
 * AddFile is a button component wrapped in a input[type="file"] that will
 * propmt the user to choose one or more files to upload using html.
 *
 * When the user has selected the file(s) to upload, `props.onFilesChange`
 * will be called with the list of files.
 *
 * FileList: https://developer.mozilla.org/en-US/docs/Web/API/FileList
 *
 * AddFileProps {
 *   onFilesChange: (files: FileList) => void
 * }
 *
 * @param {AddFileProps} props
 */
const AddFile = ({ classes, onFilesChange }) => {
  const handleInputChange = (event) => {
    const files = Array.from(event.target.files)
    onFilesChange(files)
  }

  return (
    <label htmlFor='add-file-input'>
      <input
        id='add-file-input'
        type='file'
        multiple
        className={classes.input}
        onChange={handleInputChange}
      />
      <Button component='span'>
        <AddIcon className={classes.leftIcon} />
        {i18n.t("storage_addFilesButton")}
      </Button>
    </label>
  )
}

const styles = theme => ({
  input: {
    display: 'none'
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

export default withStyles(styles)(AddFile)
