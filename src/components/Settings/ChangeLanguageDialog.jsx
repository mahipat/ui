import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'

import SafeLinkButton from '../SafeLinkButton'
import SafeLink from '../SafeLink'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'

import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import { patchSettings } from '../../worker/settings'
import {
  RELOAD_NOTIFICATION_SETTINGS,
} from '../../notifications'

import i18n from '../../../i18n'

const styles = (theme) => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 240,
  },
  dialog: {
    maxWidth: '310px',
  }
})

const CONTRIBUTE_URL = "https://www.transifex.com/welcome/"

@withSnackbar
@withStyles(styles)
export default class ChangeLanguageDialog extends React.Component {
  state = {
    language: i18n.language,
    changed: false
  }

  onChange = (e) => {
    this.setState({ language: e.target.value })
  }

  handleSave = () => {
    console.debug("[SPA] Changing language to: ", this.state.language)

    return i18n.changeLanguage(this.state.language)
      .then(() => patchSettings({ language: this.state.language, changed: true }))
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_reloading"), RELOAD_NOTIFICATION_SETTINGS)
        console.debug('[SPA] Reloading')
        setTimeout(() => { window.location.reload() }, 2000)
      })
      .catch((err) => {
        console.error("[SPA] Error changing the language:", err)
      })
  }


  render() {
    const { classes, isOpen, handleCloseButtonClick } = this.props
    const { changed, language } = this.state

    return (
      <Dialog
        open={isOpen || false}
        onClose={this.handleClose}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>Change Language</DialogTitle>
        <DialogContent className={classes.dialog}>
        <Typography variant='body2'>
            {i18n.t("dialog_language_select")} {' '}
            <SafeLink href={CONTRIBUTE_URL}>
                {i18n.t("dialog_language_contribute")}
            </SafeLink>
          </Typography>
          <br />
          <form autoComplete="off">
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="lng-select">Language</InputLabel>
              <Select value={language} onChange={this.onChange}>
                <MenuItem value="en">English</MenuItem>
                <MenuItem value="zh">Chinese</MenuItem>
                <MenuItem value="da">Danish</MenuItem>
                <MenuItem value="de">German</MenuItem>
                {/* <MenuItem value="eo">Esperanto</MenuItem> */}
                <MenuItem value="fa">Persian</MenuItem>
                <MenuItem value="fr">French</MenuItem>
                <MenuItem value="it">Italiano</MenuItem>
                <MenuItem value="ru">Russian</MenuItem>
                <MenuItem value="sv">Swedish</MenuItem>
              </Select>
            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button disabled={changed} onClick={handleCloseButtonClick}>
            {i18n.t('dialog_cancelButton') || "Cancel"}
          </Button>
          <Button disabled={changed} onClick={this.handleSave} color='primary' variant='outlined'>
            {i18n.t('dialog_saveAndReload') || "Save and Reload"}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}