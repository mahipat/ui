import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'

import SafeLink from '../SafeLink'
import SafeLinkButton from '../SafeLinkButton'

import OrionLogo from '../../assets/orion-icon.svg'
import SiderusFullLogo from '../../assets/siderus-full-logo.svg'
import pjson from  '../../../package.json'
import { eventTrack } from '../../worker/activities'

import i18n from '../../../i18n'

const WEBSITE_URL = "https://orion.siderus.io/"
const SUPPORT_URL = "https://siderus.io/support"
const CHAT_URL = "https://matrix.to/#/#siderus-orion:matrix.org"
const CHANGELOG_URL = "https://gitlab.com/siderus/orion/shell/blob/master/CHANGELOG.md"

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

class VersionDialog extends React.Component {
  componentDidMount() {
    eventTrack('about')
  }

  render () {
    const { classes, isOpen, handleCloseButtonClick} = this.props
    const date = new Date()


    return (
      <Dialog maxWidth='sm' open={isOpen || false} scroll='body' className={classes.dialog}>
        <DialogContent className={classes.content}>
          <img src={OrionLogo} alt='Orion logo' width='150px' height='150px' />
          <DialogContentText>
            <h1>{i18n.t("app_title")}</h1>
            <p>
              {isElectron && `${i18n.t("dialog_about_appVersion")} ${window.electron.remote.app.getVersion()}`}
              {isElectron && <br /> }
              {i18n.t("dialog_about_uiVersion")}: {pjson.version} ({pjson.hash})<br />
              {i18n.t("dialog_about_license")}: <SafeLink href={pjson.licenseURL}>{pjson.license}</SafeLink><br />
              Copyright 2015-{date.getFullYear()+1} <SafeLink href={pjson.author.url}>{pjson.author.name}</SafeLink> <br />
              <br />
              {i18n.t("dialog_about_madeBy")}
            </p>
            <SafeLink href={pjson.author.url}>
              <img src={SiderusFullLogo} alt='Siderus logo' width='250px' height='80px' />
            </SafeLink>
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.actions}>
          <SafeLinkButton href={WEBSITE_URL}>{i18n.t("dialog_about_website")}</SafeLinkButton>
          <SafeLinkButton href={SUPPORT_URL}>{i18n.t("dialog_about_support")}</SafeLinkButton>
          <SafeLinkButton href={CHAT_URL}>{i18n.t("dialog_about_chat")}</SafeLinkButton>
          <SafeLinkButton href={CHANGELOG_URL}>{i18n.t("dialog_about_changelog")}</SafeLinkButton>
          <Button color='primary' variant='outlined' onClick={handleCloseButtonClick}>{i18n.t("dialog_closeButton")}</Button>
        </DialogActions>
      </Dialog>
    )
  }
}

const styles = {
  dialog : {
    marginTop: 'calc(3vh)'
  },
  content: {
    textAlign: 'center',
    marginBottom: 20,
  },
  greenButton: {
    color: 'white',
    background: 'forestgreen',
    '&:hover': {
      background: 'green',
    }
  }
}

export default withStyles(styles)(VersionDialog)
