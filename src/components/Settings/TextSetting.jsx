import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import TextField from '@material-ui/core/TextField'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import SaveIcon from '@material-ui/icons/Save'

export default class TextSetting extends React.Component {
  state = {
    showSaveIcon: false
  }

  handleOnChange = (e) => {
    this.setState({ showSaveIcon: true })
    this.props.onChange(e)
  }

  handleOnSave = () => {
    this.setState({ showSaveIcon: false })
    if (this.props.onSubmit) this.props.onSubmit()
  }

  render () {
    const { ...rest } = this.props
    const { showSaveIcon } = this.state
    delete rest.onChange
    delete rest.onSubmit
    delete rest.autoFocus

    return (
      <ListItem>
        <ListItemText>
          <TextField
            fullWidth
            autoFocus={showSaveIcon}
            onChange={this.handleOnChange}
            {...rest}
          />
        </ListItemText>
        <ListItemSecondaryAction>
        { showSaveIcon &&
          <IconButton onClick={this.handleOnSave} aria-label="Save">
            <SaveIcon />
          </IconButton>
          }
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
}
