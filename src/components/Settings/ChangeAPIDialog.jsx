import React from 'react'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { withSnackbar } from 'notistack'
import mafmt from 'mafmt'

import SafeLink from '../SafeLink'
import { patchSettings } from '../../worker/settings'
import { resetIPFSClient } from '../../electron/shell'
import {
  COMPLETED_SETTINGS,
  RELOAD_NOTIFICATION_SETTINGS,
} from '../../notifications'

import i18n from '../../../i18n'

const CORS_HELP_URL = 'https://gitlab.com/siderus/orion/ui/blob/master/docs/CORS.md'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@withSnackbar
export default class ChangeAPIDialog extends React.Component {
  state = {
    newApiAddress: '',
    isValid: true,
  }

  onChange = (e) => {
    const newApiAddress = e.target.value
    let isValid = false
    try {
      isValid = mafmt.TCP.matches(newApiAddress)
    } catch (err) {
      isValid = false
    }

    this.setState({ newApiAddress, isValid })
  }

  resetToLocal = () => {
    this.setState({ newApiAddress: '/ip4/127.0.0.1/tcp/5001' })
  }

  handleSave = () => {
    const { newApiAddress } = this.state
    if(!newApiAddress) return

    console.debug('[SPA] Changing IPFS node to', newApiAddress)
    return patchSettings({ ipfsApiAddress: newApiAddress })
      .then(() => {
        console.debug('[SPA] Updating to', newApiAddress)
        this.props.enqueueSnackbar(i18n.t("snack_updated", { what: 'settings' }), COMPLETED_SETTINGS)
        // Let the Shell know we changed the address
        if(isElectron) resetIPFSClient(newApiAddress)
        // The IPFS API address has changed we must refresh the page
        this.props.enqueueSnackbar(i18n.t("snack_reloading"), RELOAD_NOTIFICATION_SETTINGS)
        console.debug('[SPA] Reloading')
        setTimeout(()=>{ window.location.reload() }, 2000)
      })
  }

  render() {
    const { newApiAddress, isValid } = this.state
    const { ipfsApiAddress = '', isOpen, handleCloseButtonClick } = this.props

    return (
      <Dialog
      open={isOpen || false}
        onClose={this.handleClose}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>{i18n.t("dialog_changeAPIAddress_title")}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {i18n.t("dialog_changeAPIAddress_text")}
            <code role='link' tabIndex='0' onKeyDown={this.resetToLocal} onClick={this.resetToLocal}>
              /ip4/127.0.0.1/tcp/5001
            </code>.
            <br />
            <SafeLink href={CORS_HELP_URL}>{i18n.t("dialog_changeAPIAddress_corsLink")}</SafeLink>
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label={i18n.t("dialog_changeAPIAddress_inputLabel")}
            fullWidth
            value={newApiAddress || ipfsApiAddress}
            onChange={this.onChange}
            error={!isValid}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseButtonClick}>
            {i18n.t('dialog_cancelButton')}
          </Button>
          <Button onClick={this.handleSave} color='primary' variant='outlined'>
            {i18n.t('dialog_saveAndReload')}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}