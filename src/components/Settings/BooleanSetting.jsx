import React from 'react'

import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Switch from '@material-ui/core/Switch'


const BooleanSeting = ({ primary, secondary, onChange, checked }) => (
  <ListItem button onClick={onChange}>
    <ListItemText primary={primary} secondary={secondary} />
    <ListItemSecondaryAction>
      <Switch
        checked={checked}
        onChange={onChange}
      />
    </ListItemSecondaryAction>
  </ListItem>
)

export default BooleanSeting
