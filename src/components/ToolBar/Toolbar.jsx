import React from 'react'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  main: {
    minHeight: '64px'
  }
})

@withStyles(styles)
export default class Toolbar extends React.Component {
  render (){
    const { classes, children, className, ...props } = this.props

    return (
      <Grid className={classes.main} container direction="row" spacing={16}>
        <Grid item xs={12} className={className} {...props} >
          { children }
        </Grid>
      </Grid>
    )
  }
}