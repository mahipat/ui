import React from 'react'
import { Link } from 'react-router-dom'
import InsertLink from '@material-ui/icons/InsertLink'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import i18n from '../../../i18n'

class InternalLink extends React.Component {
  render () {
    const { disableIcon, iconComponent, ...rest } = this.props
    let { label } = this.props

    label = label || i18n.t("menu_link")

    const iconItem = (
      <ListItemIcon>
        { iconComponent || <InsertLink /> }
      </ListItemIcon>
    )

    return (
      <MenuItem
        component={Link}
        {...rest}>

        { !disableIcon && iconItem }
        { !disableIcon && <ListItemText
          inset
          primary={label}
        />}
        { disableIcon && label }
      </MenuItem>
    )
  }

}


export default InternalLink
