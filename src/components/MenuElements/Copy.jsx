import React from 'react'
import ClipboardIcon from '@material-ui/icons/FileCopy'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import copy from 'copy-to-clipboard'
import i18n from '../../../i18n'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'


class Copy extends React.Component {
  copyHashToClipboard = () => {
    if (isElectron) {
      window.electron.remote.clipboard.writeText(this.props.value)
    } else {
      copy(this.props.value)
    }
  }

  render () {
    const { key } = this.props
    let { label } = this.props

    label = label || i18n.t("menu_copy", {what: "hash"})

    return (
      <MenuItem key={key} onClick={this.copyHashToClipboard}>
        <ListItemIcon>
          <ClipboardIcon />
        </ListItemIcon>
        <ListItemText
          inset
          primary={label}
        />
      </MenuItem>
    )
  }

}


export default Copy
