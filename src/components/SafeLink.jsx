import React from 'react'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  main: {
    cursor: 'pointer',
    textDecoration: 'underline',
  },
}

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@withStyles(styles)
class SafeLink extends React.Component {
  handleOnClick = () => {
    if(isElectron) window.electron.shell.openExternal(this.props.href)
    if(this.props.onClick) this.props.onClick()
  }

  render() {
    const {children, classes, ...rest} = this.props

    // if it is not electron, render just the link
    if (!isElectron) return (
      <a target='_blank' className={classes.main} rel='noopener noreferrer' {...rest}>
        {children}
      </a>
    )

    // If it is electron
    delete rest.href
    delete rest.target
    delete rest.onClick
    return (
      <a className={classes.main}
        role='link' tabIndex='0' onKeyDown={this.handleOnClick}
        onClick={this.handleOnClick} {...rest}
      >
        {children}
      </a>
    )
  }
}

export default SafeLink
