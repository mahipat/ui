import * as Sentry from '@sentry/browser'
import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import { resetConfiguration, clearCache } from '../worker/settings'
import { eventTrack } from '../worker/activities'
import OpenInBrowser from './MenuElements/OpenInBrowser'
import ChangeAPIDialog from './Settings/ChangeAPIDialog'

import i18n from '../../i18n'

const CORS_HELP_URL = 'https://gitlab.com/siderus/orion/ui/blob/master/docs/CORS.md'

const styles = {
  dialog: {
    'z-index': 998
  }
}


@withStyles(styles)
class ErrorDialog extends React.Component {
  state = {
    solMenuOpen: false,
    showDialogChangeAPI: false,
  }

  componentDidMount() {
    eventTrack('error', {error: this.props.error.toString()})
  }

  toggleSolutionMenu = () => {
    this.setState({solMenuOpen: !this.state.solMenuOpen})
  }

  toggleChangeIPFSAPIDialog = () => {
    this.setState({ showDialogChangeAPI: !this.state.showDialogChangeAPI })
  }

  onResetConfiguration = () => {
    eventTrack('error/resetConfig')
    console.log('[Error Dialog] Resetting the configuration...')
    resetConfiguration().then(() => {
      window.location = window.location.origin + window.location.pathname
    })
  }

  onClearCache = () => {
    eventTrack('error/clearCache')
    console.log('[Error Dialog] Clearing the app cache...')
    clearCache().then(() => {
      window.location = window.location.origin + window.location.pathname
    })
  }

  onReloadClick = () => {
    eventTrack('error/reload')
    document.location.reload()
  }

  onShowReportDialog = () => {
    eventTrack('error/report')
    const eventId = Sentry.captureException(this.props.error)
    Sentry.showReportDialog({
      eventId,
      successMessage: i18n.t("dialog_error_feedbackSent")
    })
  }

  render () {
    const { error, classes } = this.props
    const { solMenuOpen, showDialogChangeAPI } = this.state

    return (
      <React.Fragment>
        <Dialog
          open
          className={classes.dialog}
          disableEscapeKeyDown
          disableBackdropClick
        >
          <DialogTitle>{i18n.t("dialog_error_title")}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {i18n.t("dialog_error_text")}
              <br/>
              <br/>
              {i18n.t("dialog_error_subtext")}
              <br/>
              <code>{error.toString()}</code>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button buttonRef={node => {
                this.anchorEl = node
              }}
              aria-owns={solMenuOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={this.toggleSolutionMenu}
            >{i18n.t("dialog_error_quickFixButton")}
            </Button>
            <Popper open={solMenuOpen} anchorEl={this.anchorEl} disablePortal>
              <Paper>
                <ClickAwayListener onClickAway={this.toggleSolutionMenu}>
                  <MenuList>
                    <OpenInBrowser label={i18n.t("menu_corsFixLinkButton")} disableIcon to={CORS_HELP_URL}/>
                    <MenuItem onClick={this.toggleChangeIPFSAPIDialog}>{i18n.t("menu_changeAPIAddressButton")}</MenuItem>
                    <MenuItem onClick={this.onResetConfiguration}>{i18n.t("menu_resetConfButton")}</MenuItem>
                    <MenuItem onClick={this.onClearCache}>{i18n.t("menu_resetCacheButton")}</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Popper>

            <Button onClick={this.onShowReportDialog}>{i18n.t("menu_reportErrorButton")}</Button>
            <Button onClick={this.onReloadClick} variant='outlined' color='primary'>{i18n.t("menu_reloadButton")}</Button>
          </DialogActions>
        </Dialog>
        <ChangeAPIDialog
          isOpen={showDialogChangeAPI}
          handleCloseButtonClick={this.handleChangeAPIToggle} />
      </React.Fragment>
    )
  }
}

export default ErrorDialog
