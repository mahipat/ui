import React from 'react'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent'
import SafeLink from '../SafeLink'

import i18n from '../../../i18n'

const PRIVACY_LINK = 'https://siderus.io/privacy'

class TrackingContent extends React.Component {
  render () {
    return (
      <DialogContent>
        <DialogContentText>
          <h2>{i18n.t("dialog_telemetrySupport_title")}</h2>
          <p>
            {i18n.t("dialog_telemetrySupport_text")}
            <ul>
              <li>{i18n.t("dialog_telemetrySupport_fir")}</li>
              <li>{i18n.t("dialog_telemetrySupport_sec")}</li>
              <li>{i18n.t("dialog_telemetrySupport_thr")}</li>
              <li>{i18n.t("dialog_telemetrySupport_fou")}</li>
              <li>{i18n.t("dialog_telemetrySupport_fif")}</li>
            </ul>
            {i18n.t("dialog_telemetrySupport_subtext")}
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <SafeLink href={PRIVACY_LINK} target="_blank">
              {i18n.t("dialog_telemetrySupport_privacyPolicyLinkText")}
            </SafeLink>.
          </p>
        </DialogContentText>
      </DialogContent>
    )
  }

}

export default TrackingContent
