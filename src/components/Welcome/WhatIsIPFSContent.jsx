import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PlayCircleOutline from '@material-ui/icons/PlayCircleOutline'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent'
import IPFSLogo from '../../assets/ipfs-logo.svg'
import SafeLinkButton from '../SafeLinkButton'
import i18n from '../../../i18n'

const VIDEO_LINK = 'https://www.youtube.com/watch?v=5Uj6uR3fp-U&utm_source=siderus-orion&ref=siderus-orion'
const IPFS_WEBSITE = 'https://ipfs.io'

class DescriptionContent extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <DialogContent className={classes.content}>
        <div className={classes.centered}>
          <img src={IPFSLogo} alt='IPFS logo' width='75px' height='75px'/>
        </div>
        <DialogContentText>
          <h2 className={classes.centered}>{i18n.t("dialog_whatIsIPFS_title")}</h2>
          <p>
          {i18n.t("dialog_whatIsIPFS_text")}
          </p>
          <h3>{i18n.t("dialog_whatIsIPFS_point_subtitle")}</h3>
          <p>
            <ul>
              <li>{i18n.t("dialog_whatIsIPFS_point_fir")}</li>
              <li>{i18n.t("dialog_whatIsIPFS_point_sec")}</li>
              <li>{i18n.t("dialog_whatIsIPFS_point_thr")}</li>
            </ul>
          </p>
          <div className={classes.centered}>
            <SafeLinkButton
              color="primary"
              variant="contained"
              className={classes.mainButton}
              href={VIDEO_LINK}
            >
              <PlayCircleOutline /> &nbsp; {i18n.t("dialog_whatIsIPFS_playVideoButton")}
            </SafeLinkButton><br />
            <SafeLinkButton
              color="primary"
              variant="outlined"
              href={IPFS_WEBSITE}
            >
              {i18n.t("dialog_whatIsIPFS_goToIPFSWebsite")}
            </SafeLinkButton>
          </div>
        </DialogContentText>
      </DialogContent>
    )
  }
}

const styles = {
  centered: {
    align: 'center',
    textAlign: 'center',
  },
  content: {},
  mainButton: {
    color: 'white',
    marginBottom: 5,
  },
}

export default withStyles(styles)(DescriptionContent)
