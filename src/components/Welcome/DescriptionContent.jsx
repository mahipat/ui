import React from "react"
import { withStyles } from "@material-ui/core/styles"
import DialogContentText from "@material-ui/core/DialogContentText"
import DialogContent from "@material-ui/core/DialogContent"
import OrionLogo from "../../assets/orion-logo.svg"
import SafeLinkButton from "../SafeLinkButton"

import i18n from '../../../i18n'

const SUBSCRIBE_LINK = "http://eepurl.com/dfB6q5"

class DescriptionContent extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <DialogContent className={classes.content}>
        <img src={OrionLogo} alt="Orion logo" height="125px" />
        <DialogContentText>
          <h2>{i18n.t("dialog_welcome_title")}</h2>
          <p className={classes.text}>
            {i18n.t("dialog_welcome_text")}
          </p>
          <p>{i18n.t("dialog_welcome_subtext")}</p>
          <SafeLinkButton
            color="primary"
            variant="contained"
            size="large"
            className={classes.subscribeButton}
            href={SUBSCRIBE_LINK}
          >
            {i18n.t("dialog_welcome_newsletterButton")}
          </SafeLinkButton>
        </DialogContentText>
      </DialogContent>
    )
  }
}

const styles = {
  content: {
    textAlign: "center",
  },
  text: {
    textAlign: "left",
  },
  subscribeButton :{
    color: 'white',
  },
}

export default withStyles(styles)(DescriptionContent)
