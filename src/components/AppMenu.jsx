import React from 'react'
import { withRouter } from 'react-router'
import { NavLink } from 'react-router-dom'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListIcon from '@material-ui/icons/List'
import SettingsIcon from '@material-ui/icons/Settings'
import NotificationsIcon from '@material-ui/icons/Notifications'
import LeakAddIcon from '@material-ui/icons/LeakAdd'
import TextFieldsIcon from '@material-ui/icons/TextFields'
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew'
import { withStyles } from '@material-ui/core/styles'
import i18n from '../../i18n'

const styles = {
  drawerPaper: {
    paddingTop: 48,
    width: 240
  },
  navButtons: {
    height: '100%'
  },
  quitButton: {
    position: 'absolute',
    bottom: 0
  }
}

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'
@withRouter
@withStyles(styles)
export default class AppMenu extends React.Component {

  quitApp = () => {
    const dialog = window.electron.remote.dialog
    const res = dialog.showMessageBox({
      type: "question",
      message: i18n.t("dialog_areUSureQuit"),
      defaultId: 0,
      buttons: [i18n.t("dialog_cancelButton"), i18n.t("dialog_quitButton")]
    })

    if(res === 1){
      window.electron.remote.app.quit()
    }
  }

  render() {
    const { classes, open, onCloseMenu } = this.props

    return (
      <Drawer
        open={open}
        onClose={onCloseMenu}
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <List component="nav" className={classes.navButtons}>
          <ListItem
            button
            component={NavLink}
            to="/"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <ListIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t("menu_TitleFiles")} />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/names"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <TextFieldsIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t("menu_TitleNames")} />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/network"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <LeakAddIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t("menu_TitleNetwork")} />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/settings"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t("menu_TitleSettings") || "Settings"} />
          </ListItem>
          <ListItem
            button
            component={NavLink}
            to="/activities"
            onClick={onCloseMenu}
          >
            <ListItemIcon>
              <NotificationsIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t("menu_TitleActivities")} />
          </ListItem>
          {isElectron &&
            <ListItem
              className={classes.quitButton}
              button
              onClick={this.quitApp}
            >
              <ListItemIcon>
                <PowerSettingsNew />
              </ListItemIcon>
              <ListItemText primary={i18n.t("dialog_quitButton")} />
            </ListItem>
          }
        </List>
      </Drawer>
    )
  }
}
