import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import OpenInBrowserIcon from '@material-ui/icons/OpenInBrowser'
import ClipboardIcon from '@material-ui/icons/FileCopy'
import DeleteIcon from '@material-ui/icons/Delete'
import ShareIcon from '@material-ui/icons/Share'
import SaveAltIcon from '@material-ui/icons/SaveAlt'
import AddIcon from '@material-ui/icons/Add'
import SafeLink from '../SafeLink'

import i18n from '../../../i18n'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@inject('FilesStore')
@observer
class PropertiesToolbar extends React.Component {
  state = {
    shareMenuAnchor: null,
  }

  handleShare = (event) => this.setState({ shareMenuAnchor: event.currentTarget })
  handleShareClose = () => this.setState({ shareMenuAnchor: null })

  render () {
    const {
      classes,
      hash,
      isPinned = false,
      FilesStore,
      onCopyToClipboard,
      onDownload,
      shareLinks,
      onUpdate,
    } = this.props
    const { shareMenuAnchor } = this.state

    return (
      <div>
        <Button onClick={onCopyToClipboard}>
          <ClipboardIcon className={classes.leftIcon} />
          {i18n.t("menu_copy", {what: "hash"})}
        </Button>
        <Button component={this.createLink(shareLinks.url)}>
          <OpenInBrowserIcon className={classes.leftIcon} />
          {i18n.t("menu_openInBrowser")}
        </Button>
        {
          isPinned
            ? (
              <Button onClick={() => FilesStore.remove(hash).then(onUpdate(false))}>
                <DeleteIcon className={classes.leftIcon} />
                {i18n.t("menu_removePin")}
              </Button>
            )
            : (
              <Button onClick={() => FilesStore.import(hash).then(onUpdate(false))}>
                <AddIcon className={classes.leftIcon} />
                {i18n.t("menu_addPin")}
              </Button>
            )
        }
        {
          isElectron &&
          <Button onClick={onDownload}>
            <SaveAltIcon className={classes.leftIcon} />
            {i18n.t("menu_download")}
          </Button>
        }
        <Button onClick={this.handleShare}>
          <ShareIcon className={classes.leftIcon} />
          {i18n.t("menu_share")}
        </Button>
        <Menu
          anchorEl={shareMenuAnchor}
          open={Boolean(shareMenuAnchor)}
          onClose={this.handleShareClose}
        >
          <MenuItem
            className={this.props.classes.menuItem}
            component={this.createLink(shareLinks.email)}>
            {i18n.t("menu_share_via", { platform: 'Email'})}
          </MenuItem>
          <MenuItem
            className={this.props.classes.menuItem}
            component={this.createLink(shareLinks.facebook)}>
            {i18n.t("menu_share_via", { platform: 'Facebook'})}
          </MenuItem>
          <MenuItem
            className={this.props.classes.menuItem}
            component={this.createLink(shareLinks.twitter)}>
            {i18n.t("menu_share_via", { platform: 'Twitter'})}
          </MenuItem>
          <MenuItem
            className={this.props.classes.menuItem}
            component={this.createLink(shareLinks.telegram)}>
            {i18n.t("menu_share_via", { platform: 'Telegram'})}
          </MenuItem>
        </Menu>
      </div>
    )
  }

  createLink = (url) => props => <SafeLink {...props} href={url} onClick={this.handleShareClose} />
}


const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  menuItem: {
    width: '100%'
  }
})

export default withStyles(styles)(PropertiesToolbar)
