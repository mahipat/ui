import React from 'react'
import Button from '@material-ui/core/Button'
import Add from '@material-ui/icons/Add'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
//
import i18n from '../../../i18n'

/**
 * AddKeyButton is a button component that will open a dialog when clicked, asking the user
 * for the name of the new key. When the user has filled a valid new name the onNewKey prop is called.
 */
class AddKeyButton extends React.Component {
  state = {
    open: false,
    name: '',
    isValid: true
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({
      open: false,
      name: '',
      isValid: true
    })
  }

  handleSubmit = () => {
    const { onNewKey } = this.props
    const { name, isValid } = this.state

    if (name && isValid) {
      onNewKey(name)
      this.handleClose()
    }
  }

  onInputChange = (event) => {
    const name = event.target.value
    const isValid = typeof name === 'string' && name.length > 0

    this.setState({ name, isValid })
  }

  render () {
    const { classes } = this.props
    const { open, name, isValid } = this.state

    return (
      <React.Fragment>
        <Button onClick={this.handleOpen}>
          <Add className={classes.leftIcon} />
          {i18n.t('keys_addKeyButton')}
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
        >
          <DialogTitle>{i18n.t('dialog_newKey_title')}</DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              {i18n.t('dialog_newKey_text')}
            </DialogContentText>
            <TextField
              error={!isValid}
              value={name}
              onChange={this.onInputChange}
              autoFocus
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>
              {i18n.t('dialog_cancelButton')}
            </Button>
            <Button
              onClick={this.handleSubmit}
              color="primary"
              variant='outlined'
              disabled={!isValid}
            >
              {i18n.t('dialog_addButton')}
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  content: {
    minWidth: 500,
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

export default withStyles(styles)(AddKeyButton)
