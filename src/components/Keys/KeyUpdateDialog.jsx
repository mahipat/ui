import React from 'react'
import { observer, inject } from "mobx-react"
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import DialogContent from '@material-ui/core/DialogContent'
import Select from '@material-ui/core/Select'
import Typography from '@material-ui/core/Typography'

import { withSnackbar } from 'notistack'
// FIXME https://github.com/nozzle/react-static/issues/816
// import { multihash as isMultiHash } from 'is-ipfs'
import { cid as isCID } from 'is-ipfs'

import {
  STARTED_SETTINGS,
  COMPLETED_SETTINGS,
  FAILED_SETTINGS,
} from '../../notifications'

import i18n from '../../../i18n'

const styles = {
  content: {
    minWidth: 500
  }
}

@withStyles(styles)
@inject('KeysStore')
@withSnackbar
@observer
export default class KeyUpdateDialog extends React.Component {
  state = {
    cid: '',
    name: '',
    isValid: true
  }

  componentDidMount () {
    this.props.KeysStore.fetch()
  }

  handleSubmit = () => {
    const { preselectedKeyName, preselectedCID } = this.props

    const keyName = preselectedKeyName || this.state.name
    const cid = preselectedCID || this.state.cid

    if (keyName && isCID(cid)) {
      this.props.enqueueSnackbar(i18n.t("snack_updating", {what: 'key' }), STARTED_SETTINGS)
      this.props.onSubmit()
      this.props.KeysStore.publish(keyName, cid).then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_updated", { what: 'key' }), COMPLETED_SETTINGS)
        this.setState({
          cid: '',
          name: '',
          isValid: true
        })
      }).catch((err)=>{
        this.props.enqueueSnackbar(i18n.t("snack_error", { err }), FAILED_SETTINGS)
      })
    } else {
      this.setState({ isValid: false })
    }
  }

  handleCIDChange = (event) => {
    const cid = event.target.value
    this.setState({ cid, isValid: isCID(cid) })
  }

  render () {
    const { cid, name, isValid } = this.state
    const { classes, open, onClose, preselectedKeyName, preselectedCID } = this.props
    const { KeysStore } = this.props



    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{i18n.t("dialog_updateKey_title")}</DialogTitle>
        <DialogContent className={classes.content}>
          <FormControl fullWidth margin='normal'>
            <InputLabel>{i18n.t("keys_ipnsNameInputLabel")}</InputLabel>
            <Select
              disabled={!!preselectedKeyName}
              value={preselectedKeyName || name}
              onChange={(event) => this.setState({ name: event.target.value })}
              error={!isValid && !preselectedKeyName}
            >
              {
                KeysStore.keys.map(key => (
                  <MenuItem key={key.name} value={key.name}>{key.name}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
          <TextField
            label={i18n.t("keys_ipfsCidInputLabel")}
            margin='normal'
            fullWidth
            value={preselectedCID || cid}
            disabled={!!preselectedCID}
            onChange={this.handleCIDChange}
            error={!isValid && !preselectedCID}
          />
          <Typography>
            {i18n.t("dialog_updateKey_noteTimeout")}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>
          {i18n.t("dialog_cancelButton")}
          </Button>
          <Button
            onClick={this.handleSubmit}
            color='primary'
            variant='outlined'
          >
          {i18n.t("dialog_publishButton")}
        </Button>
        </DialogActions>
      </Dialog>
    )
  }
}