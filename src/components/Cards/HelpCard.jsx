import React from 'react'
import Card from '@material-ui/core/Card'
import HelpIcon from '@material-ui/icons/Help'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

import CardIcon from './CardIcon'
import i18n from '../../../i18n'

const styles = () => ({
  main: {
    flex: '1',
    marginTop: 15,
  },
  title: {},
  text: {
    fontSize: 14,
    textOverflow: 'ellipsis',
  },
  card: {
    overflow: 'inherit',
    textOverflow: 'ellipsis',
    textAlign: 'right',
    padding: 16,
    minHeight: 90,
    width: 350,
  },
})

@withStyles(styles)
export default class HelpCard extends React.Component {
  state = {
    text: '',
  }
  timeout = null

  onNextTip = () => {
    const { tips = [] } = this.props

    this.setState({
      text: tips[Math.floor(Math.random()*tips.length)] || i18n.t('cards_help_defaultText')
    })
  }

  componentWillMount () {
    this.timeout = setInterval(() => {
      this.onNextTip()
    }, 30 * 1000)
    this.onNextTip()
  }

  componentWillUnmount () {
    clearInterval(this.timeout)
  }

  render (){
    const { classes, title = null } = this.props
    const { text } = this.state

    return (
      <div className={classes.main}>
        <CardIcon
          Icon={HelpIcon}
          style={{cursor: 'pointer'}} background="black"
          tooltip={i18n.t('cards_help_nextTipTooltip')}
          onClick={this.onNextTip}
        />
        <Card className={classes.card} >
            <Typography className={classes.title} color="textSecondary">
              { title || i18n.t('cards_help_defaultTitle') }
            </Typography>
            <Typography className={classes.text} variant="body1">
              { text || i18n.t('cards_help_defaultText') }
            </Typography>
        </Card>
      </div>
    )
  }
}