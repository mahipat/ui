import React from 'react'
import { observer, inject } from 'mobx-react'
import WorkIcon from '@material-ui/icons/Work'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'
import i18n from '../../../i18n'

@inject('SettingsStore')
@observer
export default class RepoSizeCard extends React.Component {
  onClick = () => {
    this.props.SettingsStore.fetch()
  }

  render (){
    const { SettingsStore } = this.props
    const { repoSize } = SettingsStore.repoInfo || { value: 0, unit: 'kb'}

    return (
      <GenericCard
        Icon={WorkIcon}
        tooltip={i18n.t('cards_reposize_tooltip')}
        title={i18n.t('cards_reposize_title')}
        style={{width: 230}}
        onClick={this.onClick}
      >
        <Typography variant="h5" component="h3">
            {repoSize.value || "0"} {repoSize.unit || "kb"}
        </Typography>
      </GenericCard>
    )
  }
}