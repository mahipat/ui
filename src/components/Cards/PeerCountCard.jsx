import React from 'react'
import { observer, inject } from 'mobx-react'
import PeopleIcon from '@material-ui/icons/People'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'
import i18n from '../../../i18n'

@inject('NetworkStore')
@observer
export default class PeerCountCard extends React.Component {
  onClick = () => {
    this.props.NetworkStore.fetch()
  }

  render (){
    const { NetworkStore } = this.props
    return (
      <GenericCard
        Icon={PeopleIcon}
        tooltip={i18n.t('cards_peercount_tooltip')}
        title={i18n.t('cards_peercount_title')}
        style={{width: 230}}
      >
        { NetworkStore.peers &&
          <Typography variant="h5" component="h3">
              {NetworkStore.peers.length || "0"}
          </Typography>
        }
      </GenericCard>
    )
  }
}