import React from 'react'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import ArrowsIcon from '@material-ui/icons/SwapVert'
import ArrowUpIcon from '@material-ui/icons/ArrowUpward'
import ArrowDownIcon from '@material-ui/icons/ArrowDownward'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'
import i18n from '../../../i18n'

const style = {
  content: {
    fontSize: '1.35em'
  }
}

@withStyles(style)
@inject('NetworkStore')
@observer
export default class BandwidthCard extends React.Component {
  render (){
    const { NetworkStore, classes } = this.props
    const bw = NetworkStore.bandwidthUsage

    return (
      <GenericCard
        Icon={ArrowsIcon}
        tooltip={i18n.t('cards_bandwidth_tooltip')}
        style={{minWidth: 235}}
      >
        <Typography variant="h5" component="h3" className={classes.content}>
            {bw.rateOut.value || "0"} {bw.rateOut.unit || "kb/s"} <ArrowUpIcon />
            <br />
            {bw.rateIn.value || "0"} {bw.rateIn.unit || "kb/s"} <ArrowDownIcon />
        </Typography>
      </GenericCard>
    )
  }
}