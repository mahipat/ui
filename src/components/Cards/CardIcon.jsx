import React from 'react'
import Card from '@material-ui/core/Card'
import { withStyles } from '@material-ui/core/styles'
import Tooltip from '@material-ui/core/Tooltip'

const styles = (theme) => ({
  card: {
    float: 'left',
    margin: '-15px 20px 0 15px',
    zIndex: 100,
    borderRadius: 3,
    background: `linear-gradient(200deg, ${theme.palette.secondary.light} 0%, ${theme.palette.primary.main} 100%)`,
    '&:hover': {
      margin: '-20px 20px 0 15px',
    }
  },
  icon: {
    float: 'right',
    width: 64,
    height: 64,
    padding: 14,
    color: '#fff',
  },
})

const CardIcon = ({ Icon, classes, onClick, tooltip="", ...rest }) => (
  <Card className={classes.card} onClick={onClick} {...rest} >
    <Tooltip interactive title={tooltip}>
      <Icon className={classes.icon} onClick={onClick} />
    </Tooltip>
  </Card>
)

export default withStyles(styles)(CardIcon)