import React from 'react'
import { observer, inject } from 'mobx-react'
import SettingsInputAntennaIcon from '@material-ui/icons/SettingsInputAntenna'
import Typography from '@material-ui/core/Typography'

import GenericCard from './GenericCard'

import i18n from '../../../i18n'

@inject('SettingsStore')
@observer
export default class KeyRepublishCard extends React.Component {
  onClick = () => {
    this.props.SettingsStore.fetch()
  }

  render (){
    const { SettingsStore } = this.props
    const { RepublishPeriod } = SettingsStore.ipfsConfig.Ipns

    return (
      <GenericCard
        Icon={SettingsInputAntennaIcon}
        tooltip={i18n.t('cards_keyrepublish_tooltip', { RepublishPeriod })}
        title={i18n.t('cards_keyrepublish_title')}
        style={{width: 230}}
        onClick={this.onClick}
      >
        <Typography variant="h5" component="h3">
            {RepublishPeriod || "4h"}
        </Typography>
      </GenericCard>
    )
  }
}