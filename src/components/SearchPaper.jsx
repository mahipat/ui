import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import Divider from '@material-ui/core/Divider'
import SearchIcon from '@material-ui/icons/Search'
import IconButton from '@material-ui/core/IconButton'

import i18n from '../../i18n


const styles = {
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    float: 'right',
  },
  input: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
}

@withStyles(styles)
export default class SearchPaper extends React.Component {
  state = {
    searchValue: ""
  }

  onChange = (e) => {
    const searchValue = e.target.value
    this.setState({ searchValue })

    // if no editing has been done just close the modal
    if (searchValue.length <= 2) {
      if(this.props.onUnFocus) this.props.onUnFocus()
      return
    }
    if(this.props.onFocus) this.props.onFocus()
    if(this.props.onChange) this.props.onChange(searchValue)
  }

  render () {
    const { searchValue } = this.state
    const { classes, className } = this.props

    return (
      <Paper className={classes.root || className} elevation={1}>
        <InputBase value={searchValue} onChange={this.onChange} className={classes.input} placeholder={i18n.t("cards_search_WithDots")} />
        <Divider className={classes.divider} />
        <IconButton className={classes.iconButton} aria-label={i18n.t("cards_search_label")}>
          <SearchIcon />
        </IconButton>
      </Paper>
    )
  }
}
