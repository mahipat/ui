export const STARTED_SETTINGS = { variant: "info", autoHideDuration: 1500 }
export const COMPLETED_SETTINGS = { variant: "success", autoHideDuration: 2500 }
export const FAILED_SETTINGS = { variant: "error", autoHideDuration: 3500 }
export const WARNING_SETTINGS = { variant: "warning", autoHideDuration: 3500 }
export const RELOAD_NOTIFICATION_SETTINGS = { variant: "warning", autoHideDuration: 5000 }
