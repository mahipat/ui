import { action, computed, observable } from "mobx"
import moment from "moment"
import { getActivityList } from "../worker/activities"

class ActivitiesStore {
  @observable public loading = true
  @observable public activities = []
  private fetching = false
  private interval = null

  @computed get areAnyInProgress() {
    return this.activities.filter((e) => {
      if (!e) { return false }
      return e.status === "in progress"},
    ).length > 0
  }

  public sortedActivities(youngFirst = true) {
    return this.activities.slice().sort((left, right) => {
      const mleft = moment(left.timestamp)
      const mright = moment(right.timestamp)

      if (youngFirst) {
        return mright.diff(mleft)
      }
      return mleft.diff(mright)
    })
  }

  @action
  public fetch = () => {
    this.fetching = true
    return getActivityList()
      .then((activities: any[]) => {
        if (!activities) { activities = [] }
        this.activities = activities
        this.loading = false
        this.fetching = false
        return Promise.resolve()
      })
  }

  @action
  public startLoop = (frequency = 1250) => {
    if (this.interval) {
      clearInterval(this.interval)
    }

    this.interval = setInterval(() => {
      if (this.fetching) {
        return
      }

      this.fetch()
    }, frequency)
  }

  @action
  public stopLoop = () => {
    clearInterval(this.interval)
  }
}

const store = new ActivitiesStore()
export default store
