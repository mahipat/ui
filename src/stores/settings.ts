import { action, observable } from "mobx"
import {
  getIPFSConfig,
  getRepo,
  getSettings,
} from "../worker/settings"

import {
  getPeer,
} from "../worker/connectivity"

class SettingsStore {
  @observable public loading = true
  @observable public settings: object = {
    // keep it in sync with worker/src/settings.js
    allowUserTracking: true,
    disableWrapping: false,
    enableQuickInfoCards: true,
    gateway: "https://siderus.io",
    ipfsApiAddress: "/ip4/127.0.0.1/tcp/5001",
    language: "en",
    settingsVersion: 1,
    skipGatewayQuery: false,
    spaVersion: "",
  }

  @observable public peerInfo: object = {
    addresses: [],
    id: "",
  }

  @observable public repoInfo: object = {
    repoSize: {
      unit: "",
      value: "",
    },
    storageMax: "",
  }

  // See: https://github.com/ipfs/go-ipfs/blob/master/docs/config.md
  @observable	public ipfsConfig: object = {
    Datastore: {
      GCPeriod: "",
      StorageMax: "",
    },
    Discovery: {
      MDNS: {
        Enabled: "",
        Interval: "",
      },
    },
    Experimental: {
      FilestoreEnabled: false,
      Libp2pStreamMounting: true,
      P2pHttpProxy: false,
      QUIC: false,
      ShardingEnabled: false,
      UrlstoreEnabled: false,
    },
    Gateway: {
      Writable: false,
    },
    Ipns: {
      RecordLifetime: "",
      RepublishPeriod: "",
      ResolveCacheSize: "", // default 128
    },
    Mounts: {
      FuseAllowOther: false,
      IPFS: "/ipfs",
      IPNS: "/ipns",
    },
    Reprovider: {
      Interval: "",
      Strategy: "", // all, pinned or roots
    },
    Routing: {
      Type: "dht",
    },
    Swarm: {
      ConnMgr: {
        GracePeriod: "30s",
        HighWater: 900,
        LowWater: 600,
      },
      DisableBandwidthMetrics: false,
      DisableNatPortMap: false,
      DisableRelay: false,
      EnableRelayHop: false,
    },
  }
  private fetching = false
  private interval = null

  @action
  public fetch = () => {
    return this.fetchAppSettings()
      .then(() => this.fetchIPFS())
  }

  @action
  public fetchAppSettings = () => {
    return getSettings()
      .then((settings: any) => {
        // set default gateway
        this.settings = settings
      })
  }

  // Fetch all the information from IPFS Daemon
  @action
  public fetchIPFS = () => {
    return Promise.all([
      getPeer(), getRepo(), getIPFSConfig(),
    ])
    .then(([peer, repo, ipfsConfig]) => {
      this.peerInfo = peer
      this.repoInfo = repo
      this.ipfsConfig = ipfsConfig

      this.loading = false
    })
  }

  @action
  public startLoop = (frequency = 2500) => {
    if (this.interval) {
      clearInterval(this.interval)
    }
    if (this.fetching) {
      return
    }
    this.fetching = true

    this.interval = setInterval(() => {
      this.fetch()
      .then(() => {
        this.fetching = false
      })
      .catch((err) => {
        console.warn("[Settings Store] Error:", err)
      })
    }, frequency)
  }

  @action
  public stopLoop = () => {
    clearInterval(this.interval)
  }
}

const store = new SettingsStore()
export default store
