import { action, observable } from "mobx"
import { getKeyList, publishIPNS, resolveKeyValues } from "../worker/keys"

class KeysStore {
  @observable public loading = true
  @observable public keys = []
  @observable public keysResolved = {}
  private fetching = false
  private fetchingKeys = false
  private interval = null

  @action
  public fetch = (forced = false) => {
    this.fetching = true
    return getKeyList().then((keys: any[]) => {
      this.keys = keys
      this.loading = false
      this.fetching = false
    })
    .then(() => this.fetchKeysValue(forced))
  }

  @action
  public fetchKeysValue = (forced = false) => {
    const idList = this.keys.map((key) => key.id)

    if (!forced && this.keys.length === Object.keys(this.keysResolved).length) {
      return Promise.resolve()
    }

    // If we are already fetching keys, skip it
    if (!forced && this.fetchingKeys) {
      return Promise.resolve()
    }
    this.fetchingKeys = true

    return resolveKeyValues(idList)
      .then((map) => {
        this.keysResolved = map
        this.fetchingKeys = false
      })
  }

  @action
  public startLoop = (frequency = 1500) => {
    if (this.interval) {
      clearInterval(this.interval)
    }

    this.interval = setInterval(() => {
      if (this.fetching) {
        return
      }

      this.fetch()
    }, frequency)
  }

  @action
  public stopLoop = () => {
    clearInterval(this.interval)
  }

  public publish = (name: string, value: string) => {
    return publishIPNS(name, value).then((anything) => {
      this.keysResolved[name] = value
      // Force a refresh of the values after loading
      this.fetchKeysValue(true)
      return Promise.resolve(anything)
    })
  }
}

const store = new KeysStore()
export default store
