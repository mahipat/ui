import React from 'react'
import List from '@material-ui/core/List'
import BooleanSeting from '../../components/Settings/BooleanSetting';
import i18n from '../../../i18n'

class FuseMountsTab extends React.Component {
  render () {
    const { Mounts, patchConfig } = this.props

    return (
      <List>
        <BooleanSeting
          primary={i18n.t("settings_experimental_fuseAllowOther_label")}
          secondary={i18n.t("settings_experimental_fuseAllowOther_placeholder")}
          checked={!!Mounts.FuseAllowOther}
          onChange={() => patchConfig('Mounts.FuseAllowOther', !Mounts.FuseAllowOther)}
        />
      </List >
    )
  }
}

export default FuseMountsTab
