import React from 'react'
import Switch from '@material-ui/core/Switch'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { withSnackbar } from 'notistack'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import LinearProgress from '@material-ui/core/LinearProgress'

import InfoIcon from '@material-ui/icons/Info'
import { resetConfiguration, clearCache, getCacheSize, runGarbageCollection } from '../../worker/settings'
import TrackingContent from '../../components/Welcome/TrackingContent'

import {
  COMPLETED_SETTINGS,
  STARTED_SETTINGS,
} from '../../notifications'
import i18n from '../../../i18n'


class PrivacyTab extends React.Component {
  state = {
    trackingInfoOpen: false,
    resetOpen: false,
    resetting: false,
    cacheSize: null
  }

  componentDidMount () {
    this.updateCacheSize()
  }

  updateCacheSize = () => {
    getCacheSize()
      .then(cacheSize => {
        this.setState({ cacheSize })
      })
  }

  openTrackingInfoDialog = () => this.setState({ trackingInfoOpen: true })
  closeTrackingInfoDialog = () => this.setState({ trackingInfoOpen: false })

  openResetDialog = () => this.setState({ resetOpen: true })
  closeResetDialog = () => this.setState({ resetOpen: false })

  handleUserTrackingChange = () => {
    const nextValue = !this.props.allowUserTracking
    this.props.patchSettings({ allowUserTracking: nextValue })
  }

  handleDeleteConfiguration = () => {
    this.props.enqueueSnackbar(i18n.t("snack_resettingConfiguration"), STARTED_SETTINGS)
    this.setState({ resetting: true })
    resetConfiguration()
      .then(() => {
        window.location = window.location.origin + window.location.pathname
      })
  }

  handleClearCache = () => {
    this.props.enqueueSnackbar(i18n.t("snack_clearingCache"), STARTED_SETTINGS)
    clearCache()
    .then(() => this.props.enqueueSnackbar(i18n.t("snack_cacheCleared"), COMPLETED_SETTINGS))
    .then(this.updateCacheSize)
  }

  handleRunGC = () => {
    this.props.enqueueSnackbar(i18n.t("snack_garbageCollecting"), STARTED_SETTINGS)
    runGarbageCollection()
    .then(() => this.props.enqueueSnackbar(i18n.t("snack_garbageCollected"), COMPLETED_SETTINGS))
    .then(this.updateCacheSize)
  }

  render () {
    const { trackingInfoOpen, resetOpen, resetting, cacheSize } = this.state
    const { allowUserTracking } = this.props

    return (
      <React.Fragment>
        <List>
          <ListItem
            button
            onClick={this.handleUserTrackingChange}
          >
            <ListItemText
              primary={i18n.t("settings_privacy_telemetry_label")}
              secondary={i18n.t("settings_privacy_telemetry_placeholder")}
            />
            <ListItemSecondaryAction>
              <IconButton onClick={this.openTrackingInfoDialog}>
                <InfoIcon />
              </IconButton>
              <Switch
                checked={allowUserTracking}
                onChange={this.handleUserTrackingChange}
              />
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem
            button
            onClick={this.handleClearCache}
          >
            <ListItemText
              primary={i18n.t("settings_privacy_clearOrionCache_label")}
              secondary={i18n.t("settings_privacy_clearOrionCache_placeholder", { size: cacheSize || 0})}
            />
          </ListItem>
          <ListItem
            button
            onClick={this.handleRunGC}
          >
            <ListItemText
              primary={i18n.t("settings_privacy_runGarbageCollector_label")}
              secondary={i18n.t("settings_privacy_runGarbageCollector_placeholder", { size: cacheSize || 0})}
            />
          </ListItem>
          <ListItem
            button
            onClick={this.openResetDialog}
          >
            <ListItemText primary={i18n.t("settings_privacy_resetConfig_label")} />
          </ListItem >
        </List>
        <Dialog
          open={trackingInfoOpen}
          onClose={this.closeTrackingInfoDialog}
        >
          <TrackingContent />
          <DialogActions>
            <Button onClick={this.closeTrackingInfoDialog} color="primary">
              {i18n.t("dialog_closeButton")}
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={resetOpen}
          onClose={this.closeResetDialog}
        >
          {
            resetting &&
            <LinearProgress variant="query" />
          }
          <DialogTitle>{i18n.t("dialog_resetConfigCheck_title")}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {i18n.t("dialog_resetConfigCheck_text")}
              <br />
              {i18n.t("dialog_resetConfigCheck_alert")}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeResetDialog} disabled={resetting}>
              {i18n.t("dialog_cancelButton")}
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.handleDeleteConfiguration}
              disabled={resetting}
            >{i18n.t("dialog_resetButton")}</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment >
    )
  }
}

export default withSnackbar(PrivacyTab)
