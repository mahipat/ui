import React from 'react'
import { observer, inject } from 'mobx-react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import TextSetting from '../../components/Settings/TextSetting'
import BooleanSetting from '../../components/Settings/BooleanSetting'

import i18n from '../../../i18n'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

@inject('SettingsStore')
@observer
class FilesTab extends React.Component {
  state = {
    newStorageMax: false,
    newGCPeriod: false
  }

  openIPFSRepo = () => {
    if(window.electron.remote && this.props.SettingsStore.repoInfo.repoPath) {
      window.electron.remote.shell.openItem(this.props.SettingsStore.repoInfo.repoPath)
    }
  }

  render () {
    const {
      patchConfig,
      patchSettings,
      SettingsStore,
    } = this.props
    const { newStorageMax, newGCPeriod } = this.state
    const { skipGatewayQuery, disableWrapping, ipfsApiAddress } = SettingsStore.settings
    const { Gateway, Datastore } = SettingsStore.ipfsConfig

    return (
      <List>
        <TextSetting
          label={i18n.t("settings_files_storageMax_label")}
          placeholder={i18n.t("settings_files_storageMax_placeholder")}
          value={newStorageMax === false ? Datastore.StorageMax : newStorageMax}
          onChange={event => this.setState({ newStorageMax: event.target.value })}
          onSubmit={() => patchConfig('Datastore.StorageMax', newStorageMax)}
        />
        <TextSetting
          label={i18n.t("settings_files_garbageCollectionPeriod_label")}
          placeholder={i18n.t("settings_files_garbageCollectionPeriod_placeholder")}
          value={newGCPeriod === false ? Datastore.GCPeriod : newGCPeriod}
          onChange={event => this.setState({ newGCPeriod: event.target.value })}
          onSubmit={() => patchConfig('Datastore.GCPeriod', newGCPeriod)}
        />
        <BooleanSetting
          primary={i18n.t("settings_files_skipGatewayQuery_label")}
          secondary={i18n.t("settings_files_skipGatewayQuery_placeholder")}
          checked={skipGatewayQuery}
          onChange={() => patchSettings({ skipGatewayQuery: !skipGatewayQuery })}
        />
        <BooleanSetting
          primary={i18n.t("settings_files_EnableWrapping_label")}
          secondary={i18n.t("settings_files_EnableWrapping_placeholder")}
          checked={!disableWrapping}
          onChange={() => patchSettings({ disableWrapping: !disableWrapping })}
        />
        <BooleanSetting
          primary={i18n.t("settings_files_writableGateway_label")}
          secondary={i18n.t("settings_files_writableGateway_placeholder")}
          checked={Gateway.Writable}
          onChange={() => patchConfig('Gateway.Writable', !Gateway.Writable)}
        />
        { isElectron && ipfsApiAddress.includes("127.0.0.1") &&
          <ListItem button onClick={this.openIPFSRepo} >
            <ListItemText
              primary={i18n.t("settings_files_openRepositoryPath_label")}
              secondary={i18n.t("settings_files_openRepositoryPath_placeholder")}
            />
          </ListItem>
        }
      </List >
    )
  }
}

export default FilesTab
