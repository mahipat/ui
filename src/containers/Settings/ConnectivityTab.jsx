import React from 'react'
import List from '@material-ui/core/List'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { withSnackbar } from 'notistack'
import BooleanSeting from '../../components/Settings/BooleanSetting'
import TextSetting from '../../components/Settings/TextSetting'
import { resetIPFSClient } from '../../electron/shell'

import i18n from '../../../i18n'

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const RELOAD_NOTIFICATION_SETTINGS = { variant: 'warning', autoHideDuration: 3500 }

class ConnectivityTab extends React.Component {
  state = {
    // newMDNSInterval: false,
    // newMaxConnections: false,
    newIPNSRecordLifetime: false,
    newIPNSRepublishPeriod: false,
    newRoutingType: false,
    newApiAddress: '',
    newGateway: false,
  }

  copyToClipboard = text => {
    const element = document.createElement('textarea')
    element.value = text
    document.body.appendChild(element)
    element.select()
    document.execCommand('copy')
    document.body.removeChild(element)
  }

  handleApiAddressChange = () => {
    const { patchSettings } = this.props
    const { newApiAddress } = this.state

    if(!newApiAddress) return

    patchSettings({ ipfsApiAddress: newApiAddress })
      .then(() => {
        // Let the Shell know we changed the address
        if(isElectron) resetIPFSClient(newApiAddress)
        // The IPFS API address has changed we must refresh the page
        this.props.enqueueSnackbar(i18n.t("snack_reloading"), RELOAD_NOTIFICATION_SETTINGS)
        setTimeout(()=>{ window.location.reload() }, 2000)
      })
  }

  handleGatewayChange = () => {
    const { patchSettings } = this.props
    const { newGateway } = this.state

    if(!newGateway) return

    patchSettings({ gateway: newGateway })
  }

  render () {
    const {
      patchConfig,
      classes,
      Swarm,
      Discovery,
      Routing,
      Ipns,
      gateway,
    } = this.props

    const {
      // newMDNSInterval,
      newIPNSRecordLifetime,
      newIPNSRepublishPeriod,
      newRoutingType,
      newGateway,
      // newMaxConnections,
    } = this.state

    return (
      <React.Fragment>
        <List>
          <TextSetting
            label={i18n.t("settings_connectivity_gateway_label")}
            placeholder={i18n.t("settings_connectivity_gateway_placeholder")}
            value={newGateway === false ? gateway : newGateway}
            fullWidth
            margin='none'
            onChange={event => this.setState({ newGateway: event.target.value })}
            onSubmit={this.handleGatewayChange}
          />
        </List>
        <List>
          <Typography className={classes.subheading} variant='subtitle1'>
            {i18n.t("settings_connectivity_daemon_rememberRestart")}
          </Typography>

          <TextSetting
            label={i18n.t("settings_connectivity_ipnsRecordLifetime_label")}
            placeholder={i18n.t("settings_connectivity_ipnsRecordLifetime_placeholder")}
            value={newIPNSRecordLifetime === false ? Ipns.RecordLifetime : newIPNSRecordLifetime}
            onChange={event => this.setState({ newIPNSRecordLifetime: event.target.value })}
            onSubmit={() => patchConfig('Ipns.RecordLifetime', newIPNSRecordLifetime)}
          />
          <TextSetting
            label={i18n.t("settings_connectivity_ipnsRecordRepublish_label")}
            placeholder={i18n.t("settings_connectivity_ipnsRecordRepublish_placeholder")}
            value={newIPNSRepublishPeriod === false ? Ipns.RepublishPeriod : newIPNSRepublishPeriod}
            onChange={event => this.setState({ newIPNSRepublishPeriod: event.target.value })}
            onSubmit={() => patchConfig('Ipns.RepublishPeriod', newIPNSRepublishPeriod)}
          />
          <TextSetting
            label={i18n.t("settings_connectivity_routingType_label")}
            placeholder={i18n.t("settings_connectivity_routingType_placeholder")}
            value={newRoutingType === false ? Routing.Type : newRoutingType}
            onChange={event => this.setState({ newRoutingType: event.target.value })}
            onSubmit={() => patchConfig('Routing.Type', newRoutingType)}
          />
          {/* <TextSetting
            label={i18n.t("settings_connectivity_maxConnections_label")}
            placeholder={i18n.t("settings_connectivity_maxConnections_placeholder")}
            value={newMaxConnections === false ? Swarm.ConnMgr.HighWater : newMaxConnections}
            onChange={event => this.setState({ newMaxConnections: event.target.value })}
            onSubmit={() => patchConfig('Swarm.ConnMgr.HighWater', newMaxConnections)}
          /> */}
          <BooleanSeting
            primary={i18n.t("settings_connectivity_disableNatPortMap_label")}
            secondary={i18n.t("settings_connectivity_disableNatPortMap_placeholder")}
            checked={!!Swarm.DisableNatPortMap}
            onChange={() => patchConfig('Swarm.DisableNatPortMap', !Swarm.DisableNatPortMap)}
          />
          <BooleanSeting
            primary={i18n.t("settings_connectivity_enableRelayHop_label")}
            secondary={i18n.t("settings_connectivity_enableRelayHop_placeholder")}
            checked={!!Swarm.EnableRelayHop}
            onChange={() => patchConfig('Swarm.EnableRelayHop', !Swarm.EnableRelayHop)}
          />
          <BooleanSeting
            primary={i18n.t("settings_connectivity_DisableRelay_label")}
            secondary={i18n.t("settings_connectivity_DisableRelay_placeholder")}
            checked={!!Swarm.DisableRelay}
            onChange={() => patchConfig('Swarm.DisableRelay', !Swarm.DisableRelay)}
          />
          <BooleanSeting
            primary={i18n.t("settings_connectivity_MDNSDiscovery_label")}
            secondary={i18n.t("settings_connectivity_MDNSDiscovery_placeholder")}
            checked={!!Discovery.MDNS.Enabled}
            onChange={() => patchConfig('Discovery.MDNS.Enabled', !Discovery.MDNS.Enabled)}
          />
          {/* <TextSetting
            label={i18n.t("settings_connectivity_MDNSInterval_label")}
            value={newMDNSInterval === false ? Discovery.MDNS.Interval : newMDNSInterval}
            onChange={event => this.setState({ newMDNSInterval: event.target.value })}
            onSubmit={() => patchConfig('Discovery.MDNS.Interval', parseInt(newMDNSInterval))}
          /> */}
        </List>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  subheading: {
    paddingLeft: 16,
    color: theme.palette.grey[700]
  },
})

export default withStyles(styles)(withSnackbar(ConnectivityTab))
