import React from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import IconButton from '@material-ui/core/IconButton'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import InfoIcon from '@material-ui/icons/Info'
import EditIcon from '@material-ui/icons/Edit'
import TranslateIcon from '@material-ui/icons/Translate'

import VersionDialog from '../../components/Settings/VersionDialog'
import ChangeAPIDialog from '../../components/Settings/ChangeAPIDialog'
import ChangeLanguageDialog from '../../components/Settings/ChangeLanguageDialog'
import BooleanSetting from '../../components/Settings/BooleanSetting'
import i18n from '../../../i18n'


class InformationTab extends React.Component {
  state = {
    isVersionDialogVisible: false,
    isChangeAPIDialogVisible: false,
    isChangeLanguageDialogVisible: false,
  }

  handleVersionDialogToggle = () => {
    this.setState({isVersionDialogVisible: !this.state.isVersionDialogVisible})
  }

  handleChangeAPIToggle = () => {
    this.setState({isChangeAPIDialogVisible: !this.state.isChangeAPIDialogVisible})
  }

  handleChangeLanguageToggle = () => {
    this.setState({isChangeLanguageDialogVisible: !this.state.isChangeLanguageDialogVisible})
  }

  render () {
    const {
      peerId, ipfsApiAddress,
      patchSettings, enableQuickInfoCards
    } = this.props

    const {
      isVersionDialogVisible,
      isChangeAPIDialogVisible,
      isChangeLanguageDialogVisible
    } = this.state

    return (
      <React.Fragment>
        <List>
          <ListItem>
            <ListItemText
              primary={i18n.t("settings_information_peerID_label")}
              secondary={peerId}
            />
          </ListItem >
          <ListItem>
            <ListItemText
              primary={i18n.t("settings_information_currentAPIMultiaddress_label")}
              secondary={ipfsApiAddress}
            />
            <ListItemSecondaryAction>
              <IconButton onClick={this.handleChangeAPIToggle}>
                <EditIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem >
          <BooleanSetting
            primary={i18n.t("settings_information_enableQuickInfoCards_label")}
            secondary={i18n.t("settings_information_enableQuickInfoCards_placeholder")}
            checked={enableQuickInfoCards}
            onChange={() => patchSettings({ enableQuickInfoCards: !enableQuickInfoCards})}
          />
          <ListItem
            button
            onClick={this.handleChangeLanguageToggle} >
              <ListItemText
                primary={i18n.t("settings_information_changeLanguage_label") || "Change Language"}
              />
              <ListItemSecondaryAction>
                <IconButton onClick={this.handleChangeLanguageToggle}>
                  <TranslateIcon />
                </IconButton>
              </ListItemSecondaryAction>
          </ListItem>
          <ListItem
            button
            onClick={this.handleVersionDialogToggle} >
              <ListItemText
                primary={i18n.t("settings_information_aboutOrion_label")}
                secondary={i18n.t("settings_information_aboutOrion_placeholder")}
              />
              <ListItemSecondaryAction>
                <IconButton onClick={this.handleVersionDialogToggle}>
                  <InfoIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
        </List >
        <ChangeLanguageDialog
          isOpen={isChangeLanguageDialogVisible}
          handleCloseButtonClick={this.handleChangeLanguageToggle}
        />
        <VersionDialog
          handleCloseButtonClick={this.handleVersionDialogToggle}
          isOpen={isVersionDialogVisible} />
        <ChangeAPIDialog
          isOpen={isChangeAPIDialogVisible}
          handleCloseButtonClick={this.handleChangeAPIToggle}
          ipfsApiAddress={ipfsApiAddress} />
      </React.Fragment>
    )
  }
}

export default InformationTab
