import React from 'react'
import { observer, inject } from 'mobx-react'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { HashLink } from 'react-router-hash-link'
import { withSnackbar } from 'notistack'
//

import FilesTab from './FilesTab'
import ConnectivityTab from './ConnectivityTab'
import InformationTab from './InformationTab'
import PrivacyTab from './PrivacyTab'
import ExperimentalTab from './ExperimentalTab'

import { patchSettings, patchIPFSConfig } from '../../worker/settings'
import { eventTrack } from '../../worker/activities'

import {
  COMPLETED_SETTINGS,
} from '../../notifications'

import i18n from '../../../i18n'

const RESTART_NOTIFICATION_SETTINGS = { variant: 'warning', autoHideDuration: 5000 }
const RESTART_NOTIFICATION_DELAY = 500

@inject('SettingsStore')
@observer
class Settings extends React.Component {
  componentDidMount () {
    eventTrack('settings')

    this.props.SettingsStore.fetch()
  }

  handleSettingsPatch = (partialSettings) => {
    // update the state after the settings are updated
    return patchSettings(partialSettings)
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_restartAppRequired"), COMPLETED_SETTINGS)
        this.props.SettingsStore.fetch()
      })
    }


  handleConfigPatch = (key, value) => {
    patchIPFSConfig(key, value)
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_settingsUpdated"), COMPLETED_SETTINGS)
        setTimeout(() =>{
          this.props.enqueueSnackbar(i18n.t("snack_restartAppRequired"), RESTART_NOTIFICATION_SETTINGS)
        }, RESTART_NOTIFICATION_DELAY)
        this.props.SettingsStore.fetch()
      })
  }

  render () {
    const { classes, SettingsStore } = this.props

    return (
      <React.Fragment>
      { !SettingsStore.loading && <div className={classes.root}>
        <Drawer
          variant='permanent'
          anchor='left'
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <List component='nav'>
            <ListItem button component={HashLink} to='/settings#information'>
              <ListItemText primary={i18n.t("menu_settings_information")} />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#files'>
              <ListItemText primary={i18n.t("menu_settings_files")} />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#connectivity'>
              <ListItemText primary={i18n.t("menu_settings_connectivity")} />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#privacy'>
              <ListItemText primary={i18n.t("menu_settings_privacy")} />
            </ListItem>
            <ListItem button component={HashLink} to='/settings#advanced'>
              <ListItemText primary={i18n.t("menu_settings_advanced")} />
            </ListItem>
          </List>
        </Drawer>
        <div className={classes.content}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Typography
                id='information'
                variant='subtitle1'
                gutterBottom
              >{i18n.t("menu_settings_information")}</Typography>
              <Paper elevation={4}>
                <InformationTab
                  peerId={SettingsStore.peerInfo.id}
                  patchSettings={this.handleSettingsPatch}
                  enableQuickInfoCards={SettingsStore.settings.enableQuickInfoCards}
                  ipfsApiAddress={SettingsStore.settings.ipfsApiAddress}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='files'
                variant='subtitle1'
                gutterBottom
              >{i18n.t("menu_settings_files")}</Typography>
              <Paper elevation={4}>
                <FilesTab
                  patchSettings={this.handleSettingsPatch}
                  patchConfig={this.handleConfigPatch}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='connectivity'
                variant='subtitle1'
                gutterBottom
              >{i18n.t("menu_settings_connectivity")}</Typography>
              <Paper elevation={4}>
                <ConnectivityTab
                  addresses={SettingsStore.peerInfo.addresses}
                  gateway={SettingsStore.settings.gateway}
                  Swarm={SettingsStore.ipfsConfig.Swarm}
                  Routing={SettingsStore.ipfsConfig.Routing}
                  Ipns={SettingsStore.ipfsConfig.Ipns}
                  Discovery={SettingsStore.ipfsConfig.Discovery}
                  patchSettings={this.handleSettingsPatch}
                  patchConfig={this.handleConfigPatch}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='advanced'
                variant='subtitle1'
                gutterBottom
              >{i18n.t("menu_settings_advanced")}</Typography>
              <Paper elevation={4}>
                <ExperimentalTab
                  Experimental={SettingsStore.ipfsConfig.Experimental}
                  patchConfig={this.handleConfigPatch}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography
                id='privacy'
                variant='subtitle1'
                gutterBottom
              >{i18n.t("menu_settings_privacy")}</Typography>
              <Paper elevation={4}>
                <PrivacyTab
                  allowUserTracking={SettingsStore.settings.allowUserTracking}
                  patchSettings={this.handleSettingsPatch}
                />
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div> }
      </React.Fragment>
    )
  }
}

const styles = {
  root: {},
  drawerPaper: {
    paddingTop: 48,
    width: 200
  },
  content: {
    padding: 12,
    marginLeft: 200
  }
}

export default withStyles(styles)(withSnackbar(Settings))
