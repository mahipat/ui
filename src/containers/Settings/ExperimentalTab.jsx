import React from 'react'
import List from '@material-ui/core/List'
import BooleanSeting from '../../components/Settings/BooleanSetting'

import i18n from '../../../i18n'

class Experimental extends React.Component {
  render () {
    const { Experimental, patchConfig } = this.props

    return (
      <List>
        <BooleanSeting
          primary={i18n.t("settings_experimental_filestore_label")}
          secondary={i18n.t("settings_experimental_filestore_placeholder")}
          checked={!!Experimental.FilestoreEnabled}
          onChange={() => patchConfig('Experimental.FilestoreEnabled', !Experimental.FilestoreEnabled)}
        />
        <BooleanSeting
          primary={i18n.t("settings_experimental_libp2pmount_label")}
          secondary={i18n.t("settings_experimental_libp2pmount_placeholder")}
          checked={!!Experimental.Libp2pStreamMounting}
          onChange={() => patchConfig('Experimental.Libp2pStreamMounting', !Experimental.Libp2pStreamMounting)}
        />
        <BooleanSeting
          primary={i18n.t("settings_experimental_sharding_label")}
          secondary={i18n.t("settings_experimental_sharding_placeholder")}
          checked={!!Experimental.ShardingEnabled}
          onChange={() => patchConfig('Experimental.ShardingEnabled', !Experimental.ShardingEnabled)}
        />
        <BooleanSeting
          primary={i18n.t("settings_experimental_urlStore_label")}
          secondary={i18n.t("settings_experimental_urlStore_placeholder")}
          checked={!!Experimental.UrlstoreEnabled}
          onChange={() => patchConfig('Experimental.UrlstoreEnabled', !Experimental.UrlstoreEnabled)}
        />
        <BooleanSeting
          primary={i18n.t("settings_experimental_p2pHttpProxy_label")}
          secondary={i18n.t("settings_experimental_p2pHttpProxy_placeholder")}
          checked={!!Experimental.P2pHttpProxy}
          onChange={() => patchConfig('Experimental.P2pHttpProxy', !Experimental.P2pHttpProxy)}
        />
        <BooleanSeting
          primary={i18n.t("settings_experimental_quickProtocol_label")}
          secondary={i18n.t("settings_experimental_quickProtocol_placeholder")}
          checked={!!Experimental.QUIC}
          onChange={() => patchConfig('Experimental.QUIC', !Experimental.QUIC)}
        />
      </List >
    )
  }
}

export default Experimental
