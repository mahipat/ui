import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import { withSnackbar } from 'notistack'
import Hidden from '@material-ui/core/Hidden'

import LoadingOverlay from '../components/LoadingOverlay'
import PeersList from '../components/Network/PeersList'
import PeerElement from '../components/Network/PeerElement'
import SafeLink from '../components/SafeLink'

import AddressDialog from '../components/Network/AddressDialog'
import ConnectDialog from '../components/Network/ConnectDialog'
import PingDialog from '../components/Network/PingDialog'

import Toolbar from '../components/ToolBar/Toolbar'

import PeerCountCard from '../components/Cards/PeerCountCard'
import BandwidthCard from '../components/Cards/BandwidthCard'
import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'

import { eventTrack } from '../worker/activities'
import i18n from '../../i18n

const CHAT_URL = "https://matrix.to/#/#siderus-orion:matrix.org"

const styles = {}

@withStyles(styles)
@withRouter
@withSnackbar
@inject('NetworkStore')
@observer
export default class Network extends React.Component {
  componentDidMount () {
    eventTrack('networking')

    this.props.NetworkStore.fetchKnownPeers()
    this.props.NetworkStore.startLoop()
  }

  componentWillUnmount() {
    this.props.NetworkStore.stopLoop()
  }

  render () {
    const { NetworkStore } = this.props

    return (
      <React.Fragment>
        <LoadingOverlay loading={NetworkStore.loading}>
          <Toolbar>
            <AddressDialog />
            <ConnectDialog />
            <PingDialog />
          </Toolbar>
          <CardHeader>
            <PeerCountCard />
            <BandwidthCard />
            <Hidden smDown>
              <HelpCard tips={[
                i18n.t("tips_generic_disableCards"),
                i18n.t("tips_network_helpingByStandby"),
                i18n.t("tips_network_addingPeerWillMakeFaster"),
                i18n.t("tips_generic_siderusGateway"),
                <SafeLink href="https://mastodon.social/@Siderus">{i18n.t("tips_generic_followMastodon")}</SafeLink>,
                <SafeLink href="https://orion.siderus.io/">{i18n.t("tips_generic_linkMainWebsite")}</SafeLink>,
                <SafeLink href={CHAT_URL}>{i18n.t("tips_generic_linkChatroom")}</SafeLink>,
              ]}/>
            </Hidden>
          </CardHeader>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Paper elevation={4}>
                <PeersList >
                  { NetworkStore.knownPeers.map((el, index) => <PeerElement
                    peer={el} key={index} id={index}
                  />)}
                </PeersList>
              </Paper>
            </Grid>
          </Grid>
        </LoadingOverlay>
      </React.Fragment>
    )
  }
}