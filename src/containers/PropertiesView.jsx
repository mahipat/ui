import React from 'react'
import { observer, inject } from 'mobx-react'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import EditIcon from '@material-ui/icons/Edit'
import IconButton from '@material-ui/core/IconButton'
import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import Hidden from '@material-ui/core/Hidden'
import copy from 'copy-to-clipboard'
//
import LoadingOverlay from '../components/LoadingOverlay'

import ConnectedStorageList from './ConnectedStorageList'
import DescribeElementDialog from '../components/StorageList/DescribeElementDialog'
import PropertiesToolbar from '../components/Properties/PropertiesToolbar'
import CustomQRCode from '../components/Properties/CustomQRCode'
import {
  getHashProperties, describeElement, downloadFile,
} from '../worker/files'
import { eventTrack } from '../worker/activities'

import i18n from '../../i18n

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'

const SHARE_TEXT = i18n.t("share_defaultText")
const SHARE_TEXT_TWITTER = `${i18n.t("share_using")} %40IPFSRocks %23Siderus %23Orion %23IPFS`

const styles = {
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
}

/*
 * Properties is the container that will show a single CID's property.
 * It relies on the web worker to be available to retrieve the information from
 * the IPFS node and show them to the user.
 *
 * If the DAG of the CID contains links and is a directory, it will show a list
 * of links (aka Files) as in the main view with the full Storage List.
 */
@withRouter
@withStyles(styles)
@inject('SettingsStore')
@inject('FilesStore')
@observer
export default class Properties extends React.Component {
  state = {
    loading: false,
    loadingText: i18n.t("status_fetchingObjectData"),
    hash: '',
    dag: undefined,
    stat: undefined,
    isDirectory: undefined,
    describeDialogOpen: false,
    isPinned: false,
    shareLinks: {
      url: '',
      facebook: '',
      twitter: '',
      email: '',
      telegram: ''
    }
  }

  getShareLinks = (hash, gateway) => {
    const shareUrl = `${gateway}/ipfs/${hash}`
    return {
      email:  `mailto:?body=${shareUrl}%0A%0A${SHARE_TEXT}`,
      facebook: `https://www.facebook.com/sharer.php?u=${shareUrl}`,
      telegram: `https://telegram.me/share/url?url=${shareUrl}&text=${SHARE_TEXT}`,
      twitter: `https://twitter.com/intent/tweet?text=${shareUrl} ${SHARE_TEXT_TWITTER}`,
      url: shareUrl,
    }
  }

  componentWillMount () {
    eventTrack('properties')
    this.fetch()
  }

  componentDidUpdate () {
    if(this.props.match.params.cid !== this.state.hash && !this.state.loading)
      this.fetch()
  }

  fetch = (showLoading = true) => {
    const { FilesStore, SettingsStore } = this.props

    this.setState({loading: showLoading})
    const hash = this.props.match.params.cid
    const gateway = SettingsStore.settings.gateway

    SettingsStore.fetch()
    .then(() => FilesStore.isPinned(hash))
    .then((isPinned) => {
      let loadingText = i18n.t("status_fetchingObjectData")
      if (!isPinned){
        loadingText = i18n.t("status_fetchingDataFromNetwork")
      } else {
        loadingText = i18n.t("status_fetchingDataFromLocal")
      }

      this.setState({loading: showLoading, loadingText, isPinned})
      return getHashProperties(hash, gateway)
    })
    .then((EnhancedObject) => {
      const shareLinks = this.getShareLinks(hash, gateway)
      this.setState({...EnhancedObject, shareLinks, hash, loading: false})
    })
  }

  handleArrowBackClicked = () => {
    this.props.history.goBack()
  }

  handleDescribeClicked = () => this.setState({ describeDialogOpen: true })

  handleDescribe = (hash, newDescription) => {
    console.debug('[SPA] Editing description of ', hash, ' to ', newDescription)

    describeElement(hash, newDescription)
      .then(result => {
        console.debug('[SPA] Describe success ', result)
        this.setState({ description: result })
      })

    this.handleDescribeClose()
  }

  handleDescribeClose = () => {
    this.setState({ describeDialogOpen: false })
  }

  copyHashToClipboard = () => {
    if (isElectron) {
      window.electron.remote.clipboard.writeText(this.state.hash)
    } else {
      copy(this.state.hash)
    }
  }

  handleDownload = () => {
    console.debug('[SPA] Downloading hash', this.state.hash)
    downloadFile(this.state.hash)
      .then(result => {
        console.debug('[SPA] Downloading hash', result)
      })
  }

  render () {
    const {
      shareLinks, describeDialogOpen, description, isPinned,
      hash, stat, dag, isDirectory, loading, loadingText
    } = this.state
    const { classes } = this.props

    return (
      <React.Fragment>
        <LoadingOverlay loading={loading}
          text={i18n.t("status_loading")}
          subtext={loadingText}>

          <Grid container spacing={16}>
            <Grid item xs={12} className={classes.toolbar}>
              <IconButton onClick={this.handleArrowBackClicked}>
                <ArrowBackIcon />
              </IconButton>
              <PropertiesToolbar
                shareLinks={shareLinks}
                hash={hash}
                isPinned={isPinned}
                onCopyToClipboard={this.copyHashToClipboard}
                onDownload={this.handleDownload}
                onUpdate={this.fetch}
              />
            </Grid>
            { /* Properties header and QRCode */ }
            <Grid item xs={12}>
              <Grid container
                spacing={16}
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Grid item xs={12} md={9}>
                  <Paper elevation={4}>
                    <Toolbar>
                      <Typography variant="h6">{i18n.t("properties_title")}</Typography>
                    </Toolbar>
                    {
                      stat && dag && stat.CumulativeSize &&
                      <Table>
                        <TableBody>
                          <TableRow>
                            <TableCell>{i18n.t("properties_table_header_hash")}</TableCell>
                            <TableCell>{hash}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{i18n.t("properties_table_header_description")}</TableCell>
                            <TableCell>
                              {description || ' '}
                              <IconButton onClick={this.handleDescribeClicked}>
                                <EditIcon />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{i18n.t("properties_table_header_size")}</TableCell>
                            <TableCell>{stat.CumulativeSize.value} {stat.CumulativeSize.unit}</TableCell>
                          </TableRow>
                          {isDirectory && stat.NumLinks !== undefined &&
                            <TableRow>
                              <TableCell>{i18n.t("properties_table_header_numberDagLinks")}</TableCell>
                              <TableCell>{stat.NumLinks}</TableCell>
                            </TableRow>
                          }
                        </TableBody>
                      </Table>
                    }
                  </Paper>
                </Grid>
                <Hidden smDown>
                  <Grid item md={3}>
                    { !loading && /* Skip QRCode if it is loading */
                      <CustomQRCode url={shareLinks.url} />
                    }
                  </Grid>
                </Hidden>
              </Grid>
            </Grid>
            { /* End Properties header and QRCode */ }
            {
              isDirectory && dag.links.length > 0 &&
              <Grid item xs={12}>
                <Paper elevation={4}>
                  <ConnectedStorageList motherHash={hash} title={i18n.t("properties_linksFilesTitle")} list={dag.links} />
                </Paper>
              </Grid>
            }
          </Grid>
        </LoadingOverlay>
        <DescribeElementDialog
          open={describeDialogOpen}
          cid={hash}
          description={description}
          onClose={this.handleDescribeClose}
          onSubmit={this.handleDescribe}
        />
      </React.Fragment>
    )
  }
}
