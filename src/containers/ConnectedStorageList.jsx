import React from 'react'
import { withRouter } from 'react-router'
import { withSnackbar } from 'notistack'
//
import StorageList from '../components/StorageList/StorageList'
import StorageElement from '../components/StorageList/StorageElement'
import KeyUpdateDialog from '../components/Keys/KeyUpdateDialog'
import DescribeElementDialog from '../components/StorageList/DescribeElementDialog'

import { downloadFile, removeFiles, describeElement } from '../worker/files'
import { getKeyList } from '../worker/keys'


import {
  STARTED_SETTINGS,
  COMPLETED_SETTINGS,
  FAILED_SETTINGS,
} from '../notifications'

import i18n from '../../i18n

@withRouter
@withSnackbar
export default class ConnectedStorageList extends React.Component {
  state = {
    selectedHashes: [],
    names: [],
    publishDialogOpen: false,
    hashToPublish: '',

    describeDialogOpen: false,
    elementToDescribe: ''
  }

  componentDidMount () {
    getKeyList().then(names => this.setState({ names }))
  }

  handleDownload = (hash) => {
    this.props.enqueueSnackbar(i18n.t("snack_downloading", { what: hash }), STARTED_SETTINGS)
    downloadFile(hash).then()
  }

  handleRemove = (hash) => {
    this.props.enqueueSnackbar(i18n.t("snack_removing", { what: hash }), STARTED_SETTINGS)
    removeFiles(hash)
      .then(() => {
        this.removeSelection(hash)
        this.props.enqueueSnackbar(i18n.t("snack_removed", { what: hash }), COMPLETED_SETTINGS)
      })
      .catch((err) => {
        this.props.enqueueSnackbar(i18n.t("snack_error", { err }), FAILED_SETTINGS)
      })
  }

  handlePublishRequest = (hash) => {
    this.setState({ publishDialogOpen: true, hashToPublish: hash })
  }

  handlePublishClose = () => {
    this.setState({ publishDialogOpen: false, hashToPublish: '' })
  }

  handlePublish = () => {
    this.handlePublishClose()
  }

  handleDescribeClicked = (hash) => {
    const { list } = this.props
    const element = list.find(el => el.hash === hash)

    this.setState({
      describeDialogOpen: true,
      elementToDescribe: element
    })
  }

  handleDescribe = (hash, newDescription) => {
    describeElement(hash, newDescription)
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_updated", { what: hash }), COMPLETED_SETTINGS)
      })

    this.handleDescribeClose()
  }

  handleDescribeClose = () => {
    this.setState({ describeDialogOpen: false, elementToDescribe: '' })
  }

  handleRowSelect = (hash) => {
    const isSelected = this.isRowSelected(hash)

    let nextValue

    if (isSelected) {
      // filter out this hash from the array
      nextValue = this.state.selectedHashes.filter(x => x !== hash)
    } else {
      // create a new array and add this hash at the end
      nextValue = [...this.state.selectedHashes, hash]
    }

    this.setState({ selectedHashes: nextValue })
  }

  handleSelectAll = () => {

    // all selected
    if (this.areAllSelected()) {
      // reset
      this.setState({ selectedHashes: [] })
    } else {
      // select all
      const nextValue = this.props.list.map(pin => pin.hash)
      this.setState({ selectedHashes: nextValue })
    }
  }

  handleRemoveSelected = () => {
    this.props.enqueueSnackbar(i18n.t("snack_removing"), STARTED_SETTINGS)
    removeFiles(this.state.selectedHashes)
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_removed"), COMPLETED_SETTINGS)
        this.removeSelection(this.state.selectedHashes)
      })
      .catch((err) => {
        this.props.enqueueSnackbar(i18n.t("snack_error", { err }), FAILED_SETTINGS)
      })

  }

  removeSelection = (hashes) => {
    if (!Array.isArray(hashes)) {
      hashes = [hashes]
    }

    const nextValue = this.state.selectedHashes.filter(x => !hashes.includes(x))
    this.setState({ selectedHashes: nextValue })
  }

  isRowSelected = hash => this.state.selectedHashes.indexOf(hash) !== -1
  areAllSelected = () => this.state.selectedHashes.length !== 0 && this.state.selectedHashes.length === this.props.list.length
  // if there are some selections, but not all, we need to show an indeterminate checkbox
  hasSomeSelections = () => this.state.selectedHashes.length > 0 && !this.areAllSelected()

  render () {
    const { title, list, gateway, motherHash, isPins = false } = this.props
    const {
      selectedHashes,
      publishDialogOpen,
      hashToPublish,
      names,
      describeDialogOpen,
      elementToDescribe,
    } = this.state

    return (
      <React.Fragment>
        <StorageList
          title={title}
          onSelectAll={this.handleSelectAll}
          checked={this.areAllSelected()}
          indeterminate={this.hasSomeSelections()}
          selections={selectedHashes.length}
          onRemoveSelected={this.handleRemoveSelected}
        >
          {
            list.map(element => (
              <StorageElement
                {...element}
                motherHash={motherHash}
                key={`${element.hash}-${element.path}`}
                gateway={gateway}
                isPinned={isPins}
                isSelected={this.isRowSelected(element.hash)}
                onSelect={this.handleRowSelect}
                onDownload={this.handleDownload}
                onRemove={this.handleRemove}
                onPublish={this.handlePublishRequest}
                onDescribe={this.handleDescribeClicked}
              />
            ))
          }
        </StorageList>
        <KeyUpdateDialog
          open={publishDialogOpen}
          preselectedCID={hashToPublish}
          keys={names}
          onClose={this.handlePublishClose}
          onSubmit={this.handlePublish}
        />
        <DescribeElementDialog
          open={describeDialogOpen}
          cid={elementToDescribe ? elementToDescribe.hash : ''}
          description={elementToDescribe ? elementToDescribe.description : ''}
          onClose={this.handleDescribeClose}
          onSubmit={this.handleDescribe}
        />
      </React.Fragment>
    )
  }
}
