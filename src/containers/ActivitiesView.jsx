import React from 'react'
import Paper from '@material-ui/core/Paper'
import { withSnackbar } from 'notistack'
import { observer, inject } from "mobx-react"

//
import ActivityList from '../components/Activities/ActivityList'
import ActivityElement from '../components/Activities/ActivityElement'

import {
  COMPLETED_SETTINGS,
} from '../notifications'

import { clearActivities, eventTrack } from '../worker/activities'
import i18n from '../../i18n

@withSnackbar
@inject('ActivityStore')
@observer
class Activities extends React.Component {
  state = {
    youngFirst: true
  }
  componentDidMount () {
    eventTrack('activities')
  }

  handleClear = () => {
    eventTrack('activities/clear', { amount: this.props.ActivityStore.activities.length || 0})
    clearActivities()
      .then(() =>
        this.props.enqueueSnackbar(i18n.t("snack_activitiesCleared"), COMPLETED_SETTINGS)
      )
  }

  handleInvertOrder = () => {
    this.setState({ youngFirst: !this.state.youngFirst })
  }

  render () {
    const { ActivityStore } = this.props
    const { youngFirst } = this.state
    return (
      <Paper>
        <ActivityList
          onClear={this.handleClear}
          onInvertOrder={this.handleInvertOrder} sortDirection={youngFirst ? "asc": "desc"}
        >
          {
            ActivityStore.sortedActivities(youngFirst).map((activity, index) => <ActivityElement key={index} {...activity} />)
          }
        </ActivityList>
      </Paper>
    )
  }
}

export default Activities
