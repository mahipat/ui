import React from 'react'
import { observer, inject } from "mobx-react"
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Hidden from '@material-ui/core/Hidden'
//
import ConnectedKeyList from './ConnectedKeyList'

import { eventTrack } from '../worker/activities'
import { addKey } from '../worker/keys'
import SafeLink from '../components/SafeLink'

import AddKeyButton from '../components/Keys/AddKeyButton'
import ResolveKey from '../components/Keys/ResolveKey'
import Toolbar from '../components/ToolBar/Toolbar'

import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'
import KeyLifetimeCard from '../components/Cards/KeyLifetimeCard'
import KeyRepublishCard from '../components/Cards/KeyRepublishCard'
import i18n from '../../i18n

const CHAT_URL = "https://matrix.to/#/#siderus-orion:matrix.org"

const styles = theme => ({
  leftIcon: {
    marginRight: theme.spacing.unit
  }
})

@withStyles(styles)
@inject('KeysStore')
@inject('SettingsStore')
@observer
export default class Names extends React.Component {
  componentDidMount () {
    eventTrack('keys')
    this.props.SettingsStore.fetch()
    .then(() => {
      this.props.KeysStore.startLoop()
    })
  }

  componentWillUnmount () {
    this.props.KeysStore.stopLoop()
  }

  handleKeyAdd = (name) => {
    addKey(name)
      .catch((err) => {
        console.error("[SPA] Error adding the key with name:", name, err)
      })
  }

  render () {
    const { KeysStore, SettingsStore } = this.props

    return (
      <React.Fragment>
        <Toolbar>
          <AddKeyButton onNewKey={this.handleKeyAdd} />
          <ResolveKey />
        </Toolbar>
        <CardHeader>
          <KeyLifetimeCard />
          <KeyRepublishCard />
          <Hidden smDown>
            <HelpCard tips={[
              i18n.t("tips_generic_disableCards"),
              i18n.t("tips_keys_equivalenteNameResolve"),
              i18n.t("tips_keys_ipnsExpireOpenTheAppOften"),
              i18n.t("tips_keys_ipnsExpireOpenTheAppOften"),
              i18n.t("tips_keys_whatAreKeysNames"),
              i18n.t("tips_files_filesAreUnencripted"),
              i18n.t("tips_network_addingPeerWillMakeFaster"),
              i18n.t("tips_network_helpingByStandby"),
              <SafeLink href="https://twitter.com/IPFSRocks">{i18n.t("tips_generic_followTwitter")}</SafeLink>,
              <SafeLink href="https://mastodon.social/@Siderus">{i18n.t("tips_generic_followMastodon")}</SafeLink>,
              <SafeLink href={CHAT_URL}>{i18n.t("tips_generic_linkChatroom")}</SafeLink>,
            ]}/>
          </Hidden>
        </CardHeader>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper elevation={4}>
              <ConnectedKeyList gateway={SettingsStore.settings.gateway} list={KeysStore.keys} valuesMap={KeysStore.keysResolved} />
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}
