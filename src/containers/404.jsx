import React from 'react'
//
import i18n from '../../i18n

export default () => (
  <div>
    <h1>{i18n.t("genericNotFound")}</h1>
  </div>
)
