import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import { withSnackbar } from 'notistack'
import TablePagination from '@material-ui/core/TablePagination'
import Hidden from '@material-ui/core/Hidden'

import ConnectedStorageList from './ConnectedStorageList'

import AddFile from '../components/StorageList/AddFile'
import AddFileElectron from '../components/StorageList/AddFileElectron'
import ImportFile from '../components/StorageList/ImportFile'
import SearchPaper from '../components/SearchPaper'
import SafeLink from '../components/SafeLink'

import { addFile } from '../worker/files'
import { eventTrack } from '../worker/activities'

import {
  STARTED_SETTINGS,
  COMPLETED_SETTINGS,
} from '../notifications'

import HelpCard from '../components/Cards/HelpCard'
import CardHeader from '../components/Cards/CardHeader'
import RepoSizeCard from '../components/Cards/RepoSizeCard'
import PeerCountCard from '../components/Cards/PeerCountCard'

import {
  addFileFromPath
} from '../electron/shell'

import i18n from '../../i18n

const isElectron = typeof window !== 'undefined' && typeof window.electron !== 'undefined'
const CHAT_URL = "https://matrix.to/#/#siderus-orion:matrix.org"

const styles = {
  topActions: {},
  customToolbar: {
    minHeight: 64,
  }
}

@withStyles(styles)
@withRouter
@withSnackbar
@inject('SettingsStore')
@inject('FilesStore')
@inject('NetworkStore')
@observer
export default class FilesView extends React.Component {
  state = {
    searchResults: [],
    rowsPerPage: 20,
    page: 0,
    isSearching: false
  }

  componentDidMount () {
    eventTrack('file')
    setupAddAppOnDrop((fileList) => {
      // Converts FileList to Array of Files
      const files = Array.from(fileList)
      if(!files || files.length === 0) {
        console.warn('[Files View] Drag and drop resulted empty')
        return
      }

      if (!isElectron) {
        return this.handleFilesChangeInHTML(files)
      }

      // within electron, we only need the paths
      this.handleFilesChangeInElectron(files)
    })

    this.props.FilesStore.startLoop()
    this.props.NetworkStore.startLoop(5000) // refresh network every 5 seconds
    this.props.SettingsStore.startLoop(7000) // refresh settings every 7 seconds
  }

  componentWillUnmount () {
    this.props.FilesStore.stopLoop()
    this.props.NetworkStore.stopLoop()
    this.props.SettingsStore.stopLoop()
  }

  onSearchChange = (value) => {
    const searchResults = this.props.FilesStore.search(value)
    this.setState({ searchResults })
  }

  onSearchFocus = () => {
    this.setState({ isSearching: true })
  }

  onSearchUnFocus = () => {
    this.setState({ isSearching: false })
  }

  /**
   * https://developer.mozilla.org/en-US/docs/Web/API/FileList
   * @param {FileList} files
   */
  handleFilesChangeInHTML = (files) => {
    console.debug('[handleFilesChangeInHTML] adding:', files)
    if (!files) {
      console.warn('Files provided is empty.')
      return
    }

    this.props.enqueueSnackbar(i18n.t("snack_adding"), STARTED_SETTINGS)
    return Promise.all(files.map(addFile))
      .then(() => {
        // const hashes = result.map(x => x.hash)
        this.props.enqueueSnackbar(i18n.t("snack_added"), COMPLETED_SETTINGS)
      })
  }

  handleFilesChangeInElectron = (files) => {
    // In case the user selected no files/directories
    if(!files) return
    const paths = files.map((x) => {
      // Note that on Electron if we are using a Dialog we will get a string
      if(typeof x === "string") return x
      // If we are not using the dialog (but drag and drop) we need the path
      return x.path
    })

    this.props.enqueueSnackbar(i18n.t("snack_adding"), STARTED_SETTINGS)
    Promise.all(paths.map(addFileFromPath))
      .then(() => {
        // const hashes = result.map(x => x.hash)
        this.props.enqueueSnackbar(i18n.t("snack_added"), COMPLETED_SETTINGS)
      })
  }

  handleFileImport = (hash) => {
    this.props.enqueueSnackbar(i18n.t("snack_importing", { what: hash }), STARTED_SETTINGS)

    this.props.FilesStore.import(hash)
      .then(() => {
        this.props.enqueueSnackbar(i18n.t("snack_imported", { what: hash }), COMPLETED_SETTINGS)
      })
  }

  handleChangePage = (event, page) => {
    this.setState({ page })
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value })
  }

  render () {
    const { FilesStore, classes } = this.props
    const { searchResults, isSearching, rowsPerPage, page } = this.state

    const data = isSearching ? searchResults : FilesStore.pins
    const pagedData = data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    return (
      <React.Fragment>
        <Grid className={classes.customToolbar} container spacing={16}>
          <Grid item xs={12} sm={7} md={8} className={classes.topActions}>
            {
              isElectron
                ? <AddFileElectron onFilesChange={this.handleFilesChangeInElectron} />
                : <AddFile onFilesChange={this.handleFilesChangeInHTML} />
            }
            <ImportFile onFileImport={this.handleFileImport} />
          </Grid>
          <Grid item xs={12} sm={5} md={4}>
            <SearchPaper onChange={this.onSearchChange} onFocus={this.onSearchFocus} onUnFocus={this.onSearchUnFocus} />
          </Grid>
        </Grid>
        <CardHeader>
          <RepoSizeCard />
          <PeerCountCard />
          <Hidden smDown>
            <HelpCard tips={[
              i18n.t("tips_generic_disableCards"),
              i18n.t("tips_files_dragAndDrop"),
              i18n.t("tips_generic_siderusGateway"),
              i18n.t("tips_generic_sharePublicLink"),
              i18n.t("tips_files_filesAreUnencripted"),
              i18n.t("tips_files_windowsIntegration"),
              i18n.t("tips_files_hostFullSite"),
              i18n.t("tips_files_pinAddEquivalent"),
              i18n.t("tips_files_protocolLabs"),
              i18n.t("tips_files_sendOnlyOnRequest"),
              i18n.t("tips_files_deduplication"),
              i18n.t("tips_keys_whatAreKeysNames"),
              i18n.t("tips_keys_ipnsExpireOpenTheAppOften"),
              i18n.t("tips_network_helpingByStandby"),
              i18n.t("tips_network_addingPeerWillMakeFaster"),
              <SafeLink href="https://siderus.io">{i18n.t("tips_generic_orionSinceOldtimes")}</SafeLink>,
              <SafeLink href="https://twitter.com/IPFSRocks">{i18n.t("tips_generic_followTwitter")}</SafeLink>,
              <SafeLink href="https://mastodon.social/@Siderus">{i18n.t("tips_generic_followMastodon")}</SafeLink>,
              <SafeLink href="https://orion.siderus.io/">{i18n.t("tips_generic_linkMainWebsite")}</SafeLink>,
              <SafeLink href={CHAT_URL}>{i18n.t("tips_generic_linkChatroom")}</SafeLink>,
            ]}/>
          </Hidden>
        </CardHeader>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper elevation={4}>
              <ConnectedStorageList isPins list={pagedData}/>
              <TablePagination
                rowsPerPageOptions={[10, 20, 50, 75, 100]}
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': i18n.t("pagination_previous"),
                }}
                nextIconButtonProps={{
                  'aria-label': i18n.t("pagination_next"),
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}

/**
 * This function will setup the document and body events to add a file on drag
 * and drop action.
 */
export function setupAddAppOnDrop (callback) {
  document.ondragover = (event) => {
    event.preventDefault()
  }

  document.body.ondrop = (event) => {
    event.preventDefault()
    if (event.dataTransfer) {
      callback(event.dataTransfer.files)
    }
  }
}
