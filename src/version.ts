import compare from "semver-compare"
import pjson from "../package.json"

export function isLatestVersion(): Promise<boolean> {
  // If we are not connected to the internet, skip the check and assume
  // we are using the latest version
  if (!navigator.onLine) {
    return Promise.resolve(true)
  }

  const { version } = pjson
  let numberVersion = version
  let channel = "production"

  // if we have a channel in the version, we should extract it
  if (version.indexOf("-") !== -1) {
    const components = version.split("-")
    channel = components[1]
    numberVersion = components[0]
  }

  let url = `https://get.siderus.io/orion/${channel}-version`
  // prod version is available under "latest-version"
  if (channel === "production") {
    url = `https://get.siderus.io/orion/latest-version`
  }

  // dev version is available under "beta-version"
  if (channel === "development") {
    url = `https://get.siderus.io/orion/beta-version`
  }

  return fetch(url)
    .then((res) => res.text())
    .then((latestVersion) => {
      // Extract the numeric number (semver) of the latestVersion
      let numberLatestVersion = latestVersion
      if (latestVersion.indexOf("-") !== -1) {
        numberLatestVersion = latestVersion.split("-")[0]
      }

      // compare(a,b)
      // If the semver string a is greater than b, return 1.
      // If the semver string b is greater than a, return -1. <--
      // If a equals b, return 0
      return Promise.resolve(compare(numberVersion, numberLatestVersion) !== -1)
    })
}
