/**
 * Ask the user to choose a directory using the native open dialog from electron
 * and return the directory path synchronously
 *
 * Example:
 * `const savingDstination = askForSavingLocation()`
 *
 * @returns {string}
 */
export function askForSavingLocation () {
  const { dialog } = window.electron.remote

  const options = {
    title: 'Where should I save?',
    properties: ['openDirectory'],
    buttonLabel: 'Save here'
  }

  // This returns an array
  const response = dialog.showOpenDialog(options)

  return response ? response[0] : null
}

/**
 * Ask the user to choose one or more files using the native open dialog from electron
 * and return the paths synchronously
 * @returns {Array<string>} paths
 */
export function askForFilePaths () {
  const { dialog } = window.electron.remote

  const options = {
    title: 'Add File',
    properties: ['openFile', 'multiSelections']
  }

  return dialog.showOpenDialog(options)
}

/**
 * Ask the user to choose one or more directories using the native open dialog from electron
 * and return the paths synchronously
 * @returns {Array<string>} paths
 */
export function askForDirectoryPaths () {
  const { dialog } = window.electron.remote

  const options = {
    title: 'Add Directory',
    properties: ['openDirectory', 'multiSelections']
  }

  return dialog.showOpenDialog(options)
}
