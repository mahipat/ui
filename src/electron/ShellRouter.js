import React from 'react'
import { withRouter } from 'react-router'
//
import { SUBSCRIPTIONS } from './shell'

@withRouter
export default class ShellRouter extends React.Component {
  lastRequestId = 0

  componentDidMount () {
    // Requests from the shell (e.g. open the import hash dialog)
    SUBSCRIPTIONS['import-from-hash'] = this.handleImportFromHashRequest
  }

  handleImportFromHashRequest = () => {
    this.lastRequestId += 1
    this.props.history.push('/', {
      request: {
        type: 'open-dialog',
        id: this.lastRequestId
      }
    })
  }

  render () {
    return null
  }
}
