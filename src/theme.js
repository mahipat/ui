import { createMuiTheme } from '@material-ui/core/styles'
import 'typeface-roboto'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      light: '#89d9ff',
      main: '#4da8fb',
      dark: '#0079c8',
      contrastText: '#000',
    },
    secondary: {
      light: '#d37cff',
      main: '#9d4bfb',
      dark: '#6714c7',
      contrastText: '#fff',
    },
  },
})

export default theme
