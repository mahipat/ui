const fs = require('fs');
const path = require('path');
const typescript = require('typescript');

const fileExt = ['.js', '.jsx', '.ts', '.tsx'];

module.exports = {
  input: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!**/node_modules/**',
  ],
  output: './',
  options: {
    debug: true,
    func: {
      list: ['i18next.t', 'i18n.t'],
      extensions: fileExt,
    },
    trans: {
      component: 'Trans',
      i18nKey: 'i18nKey',
      defaultsKey: 'defaults',
      extensions: fileExt,
      fallbackKey: function(ns, value) {
        return value;
      },
      acorn: {
        ecmaVersion: 10, // defaults to 10
        sourceType: 'module', // defaults to 'module'
        // Check out https://github.com/acornjs/acorn/tree/master/acorn#interface for additional options
      }
    },
    lngs: [
      'de',
      'da',
      'en',
      'eo',
      'es',
      'fa',
      'fr',
      'it',
      'ru',
      'sv',
      'zh',
    ],
    ns: ['locale'],
    defaultLng: 'en',
    defaultNs: 'locale',
    defaultValue: '',
    resource: {
      loadPath: 'i18n/{{lng}}.json',
      savePath: 'i18n/{{lng}}.json',
      jsonIndent: 2,
      lineEnding: '\n'
    },
    nsSeparator: false, // namespace separator
    keySeparator: false, // key separator
    interpolation: {
      prefix: '{{',
      suffix: '}}'
    }
  },
  transform: function transform(file, enc, done) {
    const extension = path.extname(file.path);
    if(file.path.indexOf(".d.ts") !== -1) return done();

    const parser = this.parser;
    let content = fs.readFileSync(file.path, enc);

    if (['.tsx','.ts'].indexOf(extension) !== -1) {
      content = typescript.transpileModule(content, {
        compilerOptions: {
          target: 'es2018',
          experimentalDecorators: true,
        },
        fileName: path.basename(file.path)
      }).outputText;
      parser.parseTransFromString(content);
      parser.parseFuncFromString(content);
    }

    done();
  }
};
