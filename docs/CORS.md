# Change IPFS cross-origin headers settings

[CORS Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)
are a group of HTTP Headers used by Browsers to limit the access
and improve to minor levels the security of web apps. `go-ipfs` have  CORS
headers set up in order to reduce access from dapps.

Siderus Orion **automatically changes the CORS headers** to ensure that the app
will work properly. This document considers edge cases, like when Orion uses an
already existing IPFS instance or a different remote server/node that is not
managed by the standalone application.

**Note**: CORS headers should not be a way to limit / secure API endpoints, as
the rules are enforced by Browsers. Any attacker can ignore such headers and
still perform requests.

You can get more information and help by joining
[Orion chatroom on Matrix](https://matrix.to/#/!NsSfDPEJrJyBUKogDd:matrix.org?via=matrix.org&via=swedneck.xyz)

## Allow every origin

A quick fix would be to allow every node to access the API. To do so you can
run the following commands:

```shell
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["*"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'
```

This will allow connections/control to the API from any website. It is suggested
if you are developing or running Orion from a different origin.

After the commands returns successfully, please be sure to restar the daemon / node
or the app.

## Limit to local and Siderus origins

There are cases where you want to limit access to only to Orion. The following
command will instruct IPFS to allow browsers that are loading from a smaller
amount of origins required by Orion.

Note that limiting access may not always work and it is NOT a way to secure
the API access to any IPFS peer.

```shell
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://localhost:8080","http://127.0.0.1:8080","https://siderus.io"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'
```

In order to run properly, Orion needs the following endpoint to be allowed by CORS Headers:

* localhost / `127.0.0.1:8080` (local gateway, used by default)
* siderus.io (may be used as a fallback)

After the commands returns successfully, please be sure to restar the daemon / node
or the app.

## Reset

In case of problems you can reset the configuration by running:

```shell
ipfs config --json API.HTTPHeaders {}
```
