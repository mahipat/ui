# Translations

This short document will provide some information for both develoeprs and
contributors to help translating and how to use the translations module.

Please note that that also by translating, modifying the code or translations
of Orion, you agree to the [current License](../LICENSE).

## How to translate

If you want to help with the translations you can head over to the [official
Transifex project](http://transifex.com/siderus/orion-ipfs/dashboard/). There
you can help by translating or improving the current text.

If you wish to test the translations, you can download the translated strings
in the `/i18n` directory and then [run the application normally](./RUN.md).

## How to Develop

If you are adding new text string to the soruce code, please consider the
following guidelines.

The translations are managed using [i18next](https://www.i18next.com). All
the translations are available in the `/i18n` directory and the main translation
used is the english one (`/i18n/en.json`). This means that all the language
files are generated from Transifex and **should not be updated manually**.

You can always download the latest translations available by running:

```bash
make i18n_download
```

### Adding new strings

When adding a new string, ensure that the labe is properly named so that
we can understand the components or the location. For example a key name that
points to a string in the [dialog for updating the current language](../src/components/Settings/ChangeLanguageDialog.jsx)
that describe how to contribute would be:

```javascript
i18n.t("dialog_language_contribute")
```

To ensure that the string will be correctly included in the translation you can
execute `i18next-scanner` by running in the terminal:

```bash
make i18n_update
```

This will update all the translations and remove untraslated strings.

### Updating transifex

Transifex is automatically updating its source from the master branch.
Updating manually it is usually not required, but the language of reference is
English and the file to upload is `/i18n/en.json`.

### Making languages available

In order to make translations available to the users, developers can enable
or disable them from the component called [ChangeLanguageDialog](../src/components/Settings/ChangeLanguageDialog.jsx).

At the same time it is important to load the translated files inside
`/i18n/index.ts` and `/i18n/Makefile` file (see the comments).

**Before publishing a new release** consider testing the translations with the
more users and validate its content if possible.