# How to run Orion locally

This short guide will show you how to start Orion locally. This process uses a
set of tools that will look for changes to the code and automatically updated.
This is very useful for developing as well for testing new features or
[translations](./TRANSLATIONS.md) on the fly.

You can get more information and help by joining
[Orion chatroom on Matrix](https://matrix.to/#/!NsSfDPEJrJyBUKogDd:matrix.org?via=matrix.org&via=swedneck.xyz)

## Dependencies

To run locally Orion UI you need the following dependencies installed locally:

* [NodeJS](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com)
* [GNU Make](https://www.gnu.org/software/make/)
* [JQ](https://stedolan.github.io/jq/)
* [Orion beta](https://orion.siderus.io/#/beta) installed

## Start Webpack

Once everythig is available, you can run from a bash shell the
following command:

```shell
make clean run
```

This will download the modules required, build Orion web worker and run a local
server (via webpack) to access the UI.

Once that is done, the terminal will watch changes on the files and rebuild the
source on the fly. You can open a modern browser to [http://localhost:3000](http://localhost:3000)
to access the Orion UI, but in order to test and access **all the features** you
need to access it from [Orion Beta](https://orion.siderus.io/#/beta).

## Access from Orion Beta

If you wish to access all the features available, you need to install the
*beta* vesrion of the standalone app available [here](https://orion.siderus.io/#/beta).

Once instaleld and running you can enable the **Developer Tools** by pressing
on your keyboard the following combinations:

* Windows and Linux: `ctrl`-`shift`-`i`
* macOS: `cmd`-`option`-`i`

Once the Developer Tools window (or panel) is open, you can select **Console**
and change the window address, by running:

```javascript
window.location.href = "http://localhost:3000"
```

The page should reload to the local version.

![Using Orion beta to develop](../assets/run-local-beta-shell.png)