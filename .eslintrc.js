module.exports = {
  rules:{
    "import/no-extraneous-dependencies": 0,
    "experimentalDecorators": 0,
  },
  extends: 'react-tools',
  env: {
    browser: true,
    jest: true
  }
}
